'''
Module to read AGCM output time series files into python numpy arrays and 
then use the CMOR python API to convert these to CF-compliant netcdf files.

The AGCM time series files are assumed to be in CCCma binary format, referred
to here as "ccc format".

To read ccc format the module relies on the cccma_py module created by Bill
Bird as part of cview. This module was made available on ppp (science network)
by simply copying the binary from my (James's) laptop to ppp. At some point 
the ncconv repo should be updated to include the source code for cccma_py, 
which Bill Bird sent to me in Jan 2019. 

Documentation for the CMOR software and its python API is at:
    https://cmor.llnl.gov/

--------------------
Author: James Anstey
Begun: Jan 2019
--------------------
'''
###############################################################################
import os
import sys
import json
import numpy as np
import time
import model_time as mt
import datetime
import pickle
import table_utils as tu
import re
import inspect
import traceback
import functools
import copy
try: 
    import cmor  
    # This is the python interface to the CMOR software, described at
    #   https://cmor.llnl.gov/
    # The import is in a try-except block so that functions from amor.py can
    # be imported into scripts that don't require the cmor module. 
except : 
    pass
###############################################################################
# Load module that reads CCCma binary format.
LIB_DIR = os.environ['PYCMOR_DEPS']
assert isinstance(LIB_DIR, (str, unicode))
assert os.path.isdir(LIB_DIR), 'Need to specify location of cccma_py module'
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)
import cccma_py
# Ensure it was imported from the expected dir (sys.path is a long list)
assert os.path.split(cccma_py.__file__)[0] == LIB_DIR, \
    'cccma_py module should be imported from {}'.format(LIB_DIR)
###############################################################################
# Define module-level variables

# Location of the CMIP6 json tables (not the CCCma json tables)
PATH_CMORTABLES = 'cmip6-cmor-tables/Tables'

# 9 Mar 2019: is the VALIDATE_CMORVAR_INFO option still useful?
VALIDATE_CMORVAR_INFO = True
# VALIDATE_CMORVAR_INFO = True  ==>  check info against what's given in the
# CMIP6 json tables.
# E.g. dimensions of a variable are passed as input. They originally come 
# from the google spreadsheet, and got in there from the data request. 
# With VALIDATE_CMORVAR_INFO = True, we check this info against the CMIP6 json 
# tables, and if it's wrong then replace it with what's found in the CMIP6 
# json tables.

# Identify the names of requested model level grids in the data request.
# These are how the data requests asks for data on AGCM model levels without
# specifying the exact type of vertical coordinate used in a model.
MODEL_LEVELS_REQ = ['alevel', 'alevhalf']

# Info on model characteristics needed by agcm_to_cmor().
# At some point would be good to import this info from some other location, 
# e.g. some central place in the CanESM5 repo that gives model characteristics.
# For now store it here. (4 Feb 2019)
# Note, in the CMIP6 json tables, the file CMIP6_CV.json contains info about
# the model.
D_MODEL = {
    'CanESM5'   : {
        # Model resolution
        'resolution'    : {
            'delt'          : 900.,  # AGCM timestep, units: seconds
            'nlon'          : 128,   # no. of longitudes
            'nlat'          : 64,    # no. of latitudes
            'spectral res'  : 'T63',
            # For AGCM model levels ('alevel'), provide the levels string
            # as given in the jobfile. 
            'alevel'        : '''
          h01="-0108"; h02="-0144"; h03="-0190"; h04="-0250"; h05="-0327";
          h06="-0426"; h07="-0550"; h08="-0708"; h09="-0904"; h10="   11";
          h11="   15"; h12="   18"; h13="   23"; h14="   28"; h15="   35";
          h16="   43"; h17="   52"; h18="   64"; h19="   77"; h20="   92";
          h21="  110"; h22="  131"; h23="  154"; h24="  181"; h25="  211";
          h26="  245"; h27="  283"; h28="  325"; h29="  370"; h30="  420";
          h31="  474"; h32="  531"; h33="  592"; h34="  648"; h35="  698";
          h36="  742"; h37="  780"; h38="  813"; h39="  841"; h40="  865";
          h41="  885"; h42="  902"; h43="  916"; h44="  930"; h45="  944";
          h46="  958"; h47="  972"; h48="  986"; h49="  995"; h50="     ";
            ''',
            'alevel units'  : '1',
            # The parameters 'coord', 'lay', 'plid' are specified in the AGCM
            # jobfile.
            'coord'         : 'ET15',
            'lay'           : 2,
            'plid'          : 50., # units: Pa
            'pref'          : 1.0132000e5, # units: Pa
            # 'p_min' is the highest pressure level we expect to get from the
            # diagnostic time series files (ccc files). In future perhaps this
            # should be a config parameter read in from a diagnostics jobfile.
            'p_min'         : 100., # units: Pa
            # d_levs_req2out specifies what are the actual type of model
            # levels. This is used to map requested levels to actual levels.
            'd_levs_req2out' : {
                'alevel'        : 'alternate_hybrid_sigma'
            ,   'alevhalf'      : 'alternate_hybrid_sigma_half'
            },
            # Soil depth
            'sdepth_bnds'   : np.array([[0., 0.1],[0.1,.35],[.35,4.1]]), # top & bottom of each soil layer
            'sdepth'        : np.array([ 0.1, 0.35, 4.1 ]), # layer bottoms
            'sdepth units'  : 'm',
        },
        # Date at which the model starts running
        # (NOT the time axis reference date! This is a model property.)
        'model_start'   : {'year' : 1, 'month' : 1, 'day' : 1},
    }
}
# CanOE variant uses identical agcm params
D_MODEL['CanESM5-CanOE'] = D_MODEL['CanESM5']

model = 'CanESM5-1' # CanESM5.1 registered source_id (dots aren't allowed)
D_MODEL[model] = copy.deepcopy(D_MODEL['CanESM5'])
D_MODEL[model]['resolution']['alevel'] = '''
                h01="-0108"; h02="-0144"; h03="-0190"; h04="-0250"; h05="-0327";
                h06="-0426"; h07="-0550"; h08="-0708"; h09="-0904"; h10="99115";
                h11="99145"; h12="99182"; h13="99228"; h14="99283"; h15="99350";
                h16="99430"; h17="99525"; h18="99637"; h19="99769"; h20="99923";
                h21="  110"; h22="  131"; h23="  154"; h24="  181"; h25="  211";
                h26="  245"; h27="  283"; h28="  325"; h29="  370"; h30="  420";
                h31="  474"; h32="  531"; h33="  592"; h34="  648"; h35="  698";
                h36="  742"; h37="  780"; h38="  813"; h39="  841"; h40="  865";
                h41="  885"; h42="  902"; h43="  916"; h44="  930"; h45="  944";
                h46="  958"; h47="  972"; h48="  986"; h49="  995"; h50="     ";
            '''
# CanESM5.1 levels were obtained from
#   https://gitlab.science.gc.ca/CanESM/CanESM5/blob/develop_canesm/CONFIG/ESM/model_run_basefile_defaults.cfg
#   commit: 50eab178e68443717369d7a35315a1006aea79fb
del model

###############################################################################
# Stuff for error logging

# Default location and name for logfile
# (if needed, the calling script can change it)
logfile_dir = './amor_logs'
logfile = 'amor.log'    # default name for logfile
logfile = os.path.join(logfile_dir, logfile) # full path to logfile
# This logfile catches info & errors that are specifically to do with amor.py 
# functionality, but not CMOR functionality (the cmor module has its own 
# logfile output).
def msg(w, excep=False):
    '''Write messages to std out and/or logfile.
        
    Messages are appended to the logfile. To start a fresh logfile, the 
    calling program should provide a new logfile name or remove the old
    logfile. The logfile name and location are the module attributes:
        amor.logfile
        amor.logfile_dir
    
    Parameters
    ----------
    excep : bool
        Set True if a message is informing user about a fatal error, and
        an exception will be raised.
    '''
    write_logfile = True
    show_msgs = True
    w = str(w)
    if excep: 
        w_excep = w
        w += '\nAborting'
    if show_msgs:
        # Show message on the standard out (std out).
        print(w)
    if write_logfile: 
        # Append message to logfile.
        if not os.path.isdir(logfile_dir): os.makedirs(logfile_dir)
        with open(logfile, 'a') as f:
            f.write(w + '\n')
    if excep:
        raise Exception(w_excep)
###############################################################################
# General functions
def chksme(x):
    '''Return unique entries of list x (filter out duplicate entries).'''
    return [x[m] for m in filter(lambda m: x[m] not in x[:m], range(len(x)))]

def getsme(x):
    '''Return only repeated entries of list x.'''
    y = [x[m] for m in filter(lambda m: x[m] in x[:m], range(len(x)))]
    return y

def file_size_str(filepath):
    '''Return a string showing size of a file in convenient units (like the 
    -h option for ls does).
    
    Parameters
    ----------
    filepath : str
        Path to the file whose size is to be displayed.
    '''
    try:
        a = os.path.getsize(filepath)
    except:
        return ''
    m = 1024.
    d_b = {
        'B' : 1.
    ,   'KB': 1 / m
    ,   'MB': 1 / m**2
    ,   'GB': 1 / m**3
    ,   'TB': 1 / m**4
    ,   'PB': 1 / m**5
    }
    # list of units, in order of descending size of the unit
    uo = sorted([(d_b[s], s) for s in d_b])
    # choose the most sensible size to display
    for tu in uo:
        if (a*tu[0]) > 1: break
    su = tu[1]
    a *= tu[0]
    sa = str('%.3g' % a)
    return sa + ' ' + su

def is1D(a):
    '''Return True if input is a 1D array.'''
    return np.prod(a.shape) == np.max(a.shape)

###############################################################################
# Functions dealing with grids
def make_fake_grid(n, amin, amax):
    '''Create grid array of length n that spans range [amin, amax].
    Was used for initial testing of CMOR-izing data from ccc files (Jan 2019).
    '''
    assert amax > amin
    assert n > 0
    da = (amax - amin)/(n + 1)
    d1 = np.linspace(amin, amax,n+1)
    d2 = d1[:-1]
    d3 = d1[1:]
    a = (d2 + d3)/2.
    d2.shape = (n,1)
    d3.shape = (n,1)
    b = np.concatenate((d2,d3),1)
    return a,b

def get_lats(model):
    '''Return arrays of latitude and latitude bounds for a given model.
    
    Parameters
    ----------
    model : str
        Name that uniquely identifies the model, e.g. "CanESM5".
    '''
    
    method = 'load pkl'
    
    if method in ['load pkl']:
        # Get binary (pkl file) containing latitude bounds
        # Currently (5 Mar 2019) this is a pickle (pkl) file produced by 
        # get_gaussian_latitudes.py.
        lats_dir = os.environ['PYCMOR_DEPS']
        s_res = D_MODEL[model]['resolution']['spectral res']
        filename = 'AGCM_lats_' + s_res + '.pkl'
        filepath = os.path.join(lats_dir, filename)
        with open(filepath, 'rb') as pf:
            d = pickle.load(pf)
        lat = d['lat']
        lat_bnds = d['lat_bnds']
    else:
        raise Exception('Unknown method: {}'.format(method))
    return lat, lat_bnds

def get_bounds(a, bmin=None, bmax=None):
    '''For 1D array of grid values (e.g. latitude), return an array of
    bounds giving the midpoint values. The min,max are used to set the 
    bounds at the edges. If not given then the spacing at the edges is 
    assumed to be the same as the mean spacing in the interior, hence 
    if the grid is evenly spaced then the edge bounds will have the same
    spacing as the rest.
    
    Parameters
    ----------
    a : array
        1D grid (e.g. latitude, longitude, ...)
    bmin : float
        Minimum value of grid bounds (e.g. -90 for latitude)
    bmax : float
        Maximum value of grid bounds (e.g. +90 for latitude)
        
    Returns
    -------
    out : array 
        2D array of bounds, with shape = (len(a), 2)
        
    Example
    -------
    For the evenly spaced longitude grid
        a = np.array([ 0, 5, 10, ..., 355 ])
    then get_bounds(a) returns
        np.array([[  -2.5     2.5  ]
                  [   2.5     7.5  ]
                  [   7.5    12.5  ]
                  ...
                  [ 352.5   357.5  ]])
    In this case there was no need to manually specify bmin, bmax.
    '''
    assert len(a.shape) == 1
    if len(a) == 1:
        d1 = .05
        d2 = a[0]*np.array([ 1-d1, 1+d1 ])
        bmin = d2.min()
        bmax = d2.max()
    
    d1 = np.diff(a)
    assert all(d1 > 0) or all(d1 < 0)    # ensure monotonicity
    flip = all(d1 < 0)
    if flip: a = a[::-1]
    assert all(np.diff(a) > 0)
    
    n = len(a)
    # Get values at midpoints between the grid values
    d1 = (a[:-1] + a[1:])/2.
    # If bmin,bmax aren't given, assume values for them
    if bmin is None: bmin = a.min() - np.diff(a).mean()/2.
    if bmax is None: bmax = a.max() + np.diff(a).mean()/2.
    # Ensure valid bmin, bmax values
    assert all(a > bmin)
    assert all(a < bmax)
    # Create array giving the bounds
    b = np.concatenate((np.array([bmin]), d1), 0)
    b = np.concatenate((b, np.array([bmax])), 0)
    
    # Rearrange the bounds array into the shape desired by CMOR, which is an
    # n x 2 array (n = number of gridpoints). 
    d2 = b[:-1]
    d3 = b[1:]
    a = (d2 + d3)/2.
    d2.shape = (n,1)
    d3.shape = (n,1)
    b = np.concatenate((d2,d3),1)
    
    if flip: 
        #b = b[::-1,::-1]    
        # not sure if 2nd dim should be flipped... leave it for now (17dec.18)
        # (I don't know if CMOR allows grids that are decreasing)
        b = b[::-1,:]
    
    return b

def parse_levels_string(w, return_level_label=False):
    '''Take a model levels string as it would appear in a CCCma AGCM jobfile
    and convert it to an array of integers.
 
    Example of a model levels string:
          h01="-0108"; h02="-0144"; h03="-0190"; h04="-0250"; h05="-0327";
          h06="-0426"; h07="-0550"; h08="-0708"; h09="-0904"; h10="   11";
          h11="   15"; h12="   18"; h13="   23"; h14="   28"; h15="   35";
          h16="   43"; h17="   52"; h18="   64"; h19="   77"; h20="   92";
          h21="  110"; h22="  131"; h23="  154"; h24="  181"; h25="  211";
          h26="  245"; h27="  283"; h28="  325"; h29="  370"; h30="  420";
          h31="  474"; h32="  531"; h33="  592"; h34="  648"; h35="  698";
          h36="  742"; h37="  780"; h38="  813"; h39="  841"; h40="  865";
          h41="  885"; h42="  902"; h43="  916"; h44="  930"; h45="  944";
          h46="  958"; h47="  972"; h48="  986"; h49="  995"; h50="     ";
    (These are the CanESM5 vertical levels.)

    Parameters
    ----------
    w : str
        String giving the vertical levels, in the format shown above.

    Returns
    -------
    out : array of integers representing the vertical levels
    '''
    l1 = [s.strip() for s in w.split('\n') if s not in ['']]
    lev = []
    lev_label = []
    for s1 in l1:
        l2 = [s.strip() for s in s1.split(';') if s not in ['']]
        for s2 in l2:
            assert s2.count('=') == 1
            l = s2.partition('=')
            s = l[-1]
            s_label = l[0]
            s = s.strip('"')
            # level lists in CCCma jobfiles sometime have blank entries, 
            # e.g. h50="     ", so disregard these
            if s in [' '*len(s)]: continue 
            lev += [int(s)]
            lev_label += [s_label]
    lev = np.array(lev)
    if return_level_label:
        # Return array of levels as well as list of their string labels 
        # ("h01", "h02", etc)
        return lev, lev_label
    else:
        # Just return the array of levels
        return lev

def decode_cccma_level(y):
    '''Given a vertical level encoded in CCCma's AGCM levels format, i.e. 
    represented by an integer, decode it to a pressure value. If it's a model 
    level, this will be a 1000*eta value.

    I believe the code used in the model for this is:
    https://gitlab.science.gc.ca/CanESM/CanDIAG/blob/develop_canesm/lssub/comm/gen/lvdcode.f
    
    Parameters
    ----------
    y : int
        Integer represeting the vertical level.
    
    Note, in a ccc file the vertical level of a single horizontal field is
    given by its IBUF(4) value.
    '''
    assert isinstance(y, int)
    if y < 0:
        # Levels above 10 (0.01 eta, ~10 hPa) are represented as negative integers,
        # with the first number indicating the exponent of 10. The level must be
        # given with 3 significant figures.
        # Examples:
        #   -700 = 7*1e0 = 7.
        #   -1200 = 2*1e-1 = 0.2
        y = np.abs(y)
        assert (y <= 9999) and (y >= 100), 'integer {} is outside of valid range'.format(y)
        x = np.float(y)/1e3
        d2, d1 = np.divmod(x,1)
        x = d1 * 10**(1 - d2)
    elif (y >= 99101) and (y <= 99999):
        # Levels between 100 and 10 (0.1 to 0.01 eta, ~100 to ~10 hPa) can optionally
        # be represented with higher precision by prefixing a 3-digit integer with "99".
        # Examples:
        #   99100 = 10.0  (equivalent to "   10" without using the "99" encoding)
        #   99123 = 12.3
        x = y - 99000
        x = np.float(x)/1e1
    else:
        x = np.float(y)
    return x
###############################################################################
# Functions dealing with json tables or other data request info

def get_table_info(cmortable, tables_path=PATH_CMORTABLES):
    ''' Get info from a CMIP6 json table.
    
    Parameters
    ----------
    cmortable : str
        Name of the file containing the CMOR table info. 
        For CMIP6, these are "CMIP6_" followed by the CMOR table name, e.g. 
        "CMIP6_Amon", or similarly for other tables that don't contain info
        for specific CMOR tables, e.g. "CMIP6_coordinate". 
        The filename can include the .json extension, or not.
    tables_path : str
        Directory containing the .json files. 
        
    Returns
    -------
    out : dict 
        Dictionary containing the info from the json file. 
    '''
    ext = '.json'
    # If .json extension is part of cmortable, remove it
    if cmortable.endswith(ext): cmortable = os.path.splitext(cmortable)[0]
    
    # Get json file contents as a python dict
    tablepath = os.path.join(tables_path, cmortable + ext)
    assert os.path.exists(tablepath), 'json table path not found: '+tablepath
    with open(tablepath, 'r') as f:
        d = json.load(f)
        l = []
        l += ['file: ' + os.path.abspath(tablepath)]
        if 'Header' in d:
            dreq_version = d['Header']['data_specs_version']
            l += ['data request version: ' + dreq_version]
        #msg('Loaded contents of table: ' + str('%-20s' % cmortable) + \
        #    ' (' + ', '.join(l) + ')')
        msg('Loaded contents of json table: ' + cmortable \
            + ''.join(['\n  ' + s for s in l]))
    return d

def show_cmorvar_json_info(cmortable, cmorvar):
    '''Display info about a CMOR variable contained in a CMIP6 json file.
    
    Parameters
    ----------
    cmortable : str
        Name of file with CMOR table info, e.g. "CMIP6_Amon" or just "Amon"
    cmorvar : str
        Name of CMOR variable, e.g. "tas", "pr", "psl"
    '''
    if '_' in cmortable:
        cmortable_name = cmortable.split('_')[-1] # e.g. CMIP6_Amon --> Amon
    else:
        # Assume it's a CMIP6 json table
        cmortable_name = cmortable
        cmortable = 'CMIP6_' + cmortable
    d_cmortable = get_table_info(cmortable)
 
    d = d_cmortable['variable_entry'][cmorvar]
    dreq_version = d_cmortable['Header']['data_specs_version']
    lw = []
    lw += ['CMIP6 json table info for ' + cmorvar + '_' + cmortable_name \
        + ' (data request version: ' + dreq_version + '):']
    for t in sorted( d.items() ): 
        lw += [' '*4 + str('%-15s : %s' % t)]
    msg('\n'.join(lw))

###############################################################################
# Functions and data dealing with vertical levels.

def get_requested_z_grids(d_json):
    '''Return list of valid names of requested vertical grids.

    The grid names returned are the names as REQUESTED in the dreq. 
    This need not be the same as the grid name appearing in the json file.
    For model levels, the requested grid name is 'alevel' or 'ahalflev' but
    the json file indicates the specific grid type for the model, e.g. 
    'alternate_hybrid_sigma'.

    This function loops over specific grid types (in the json file) and uses 
    the generic_level_name attribute to find the requested grid name 
    corresponding to the specific one.
    
    Parameters
    ----------
    d_json : dict
        Contents of CMIP6_coordinate.json file, obtained by:
         d_json = get_table_info('CMIP6_coordinate')

    Returns
    -------
    out : list
        List of valid names of requested vertical grids.
    '''
    d = d_json['axis_entry']
    l = []
    for dn in d:
        if d[dn]['axis'] in ['Z']: 
            s = dn
            k = 'generic_level_name'
            if k in d[dn]: 
                if len(d[dn][k]) > 0:
                    s = d[dn][k]
            l += [str(s)]
    return chksme(sorted(l))

def get_requested_levels(d_json):
    '''Return levels and units of requested vertical grids (e.g. plev19, 
    p850, ...).

    Note that model levels have no specific requested levels since these 
    depend on the model. So the output dict from this function contains no
    entries for model levels. 
    
    Parameters
    ----------
    d_json : dict
        Contents of CMIP6_coordinate.json file, obtained by:
         d_json = get_table_info('CMIP6_coordinate')
    
    Returns
    -------
    out : dict
        Dictionary specifying the levels and units of requested vertical
        grids. Dict keys are grid names as given in the json file.
    '''
    d_coord = d_json['axis_entry']
    d_levs_req = {}
    l_lev_type = sorted([gn for gn in d_coord if d_coord[gn]['axis'] in ['Z']])
    # Entries of l_lev_type seem to correspond to the 'label' attribute of 
    # 'grids' items in the dreq
    for gn in l_lev_type:
        
        levs_req        = d_coord[gn]['requested']
        levs_req_bnds   = d_coord[gn]['requested_bounds']
        levs_val        = d_coord[gn]['value']
        levs_units      = d_coord[gn]['units']
        levs_type       = d_coord[gn]['long_name']

        levs = None
        if levs_req not in ['']:
            # json file provides a list of requested levels
            assert len(levs_req) > 0
            levs = [float(s) for s in levs_req]
        elif levs_val not in ['']:
            # json file provides a single value for a requested level
            if gn in ['p840']: 
                # I'm assuming that this is an error in the json tables 
                # (26jan.19)
                if levs_val == 'm': levs_val = '840'
            levs = [float(levs_val)]
        else:
            # no specific levels are requested. This occurs when the request
            # applies to a type of level, e.g. AGCM model levels, for which
            # specific values can't be given.
            levs = []
        if levs is not None:
            gn = str(gn)
            d_levs_req[gn] = {
                'levels' : np.array(levs), 
                'units' : levs_units, 
                'type' : levs_type
            }
            if levs_req_bnds not in ['']:
                n = len(levs)
                levs_req_bnds = np.array([float(s) for s in levs_req_bnds])
                levs_req_bnds.shape = (n,2)
                d_levs_req[gn]['bnds'] = levs_req_bnds

    return d_levs_req  

def coordab(lev, coord, plid, pref):
    '''
    Calculate A,B level coefficients as done in CanAM (AGCM component of CanESM).
    Follows subroutine COORDAB:
        https://gitlab.science.gc.ca/CanESM/CanDIAG/blob/develop_canesm/lssub/comm/gen/coordab.f
    These are the coefficients used to determine the pressure on model levels
    according to:
        p = ap + b*ps
    where
        p = pressure on model level
        ps = instantaneous surface pressure
    Here A = ap, B = b; the ap,b notation is used in the CMIP6 json files
    and appears in the metadata of the output CMOR-ized netcdf files.

    The AGCM uses a hybrid vertical coordinate following Laprise & Girard 1990
    (von Salzen et al. 2013; Scinocca et al. 2008; Beagley et al. 1997), for
    which pressure on model levels is:
        p = p0*eta + (ps - p0)*[ (eta - eta_T) / (1 - eta_T) ]**1.5
    where
        ps = instantaneous surface pressure
        p0 = reference pressure
        eta = vertical coordinate value
        eta_T = p_T / p0, where p_T is a minimum pressure value
    (This follows the notation of Beagley etal 1997, Section 2, first paragraph.)
    From this equation, by inspection the level coefficients are:
        B = ( (eta - eta_T) / (1. - eta_T) )**power
        A = p0*(eta - B)
        
    Parameters
    ----------
    lev : numpy array
        Array (of floats) giving eta values of the model levels.
        Can be in order of ascending or descending altitude.
        Example: 
            lev = array([ 0.995, 0.986, ... ])
        In COORDAB this is the ETA input array.
    coord: str
        Specifies the type of vertical coordinate, e.g. 'ET15'.
        This parameter appears in the AGCM basefile:
                coord=" ET15"     ;
        In COORDAB this is the ICOORD input parameter.
    plid : float or int
        Minimum pressure, a constant.
        Must be in same units as pref.
        This parameter appears in the AGCM basefile:
                plid="      50.0";
        In the notation of Beagley et al. 1997, this is the p_T parameter.
        In COORDAB this is the PTOIT parameter.
    pref : float or int
        Reference pressure, a constant.
        Must be in same units as plid.
        In the notation of Beagley et al. 1997, this is the p0 parameter.
        In COORDAB this is the P0 parameter, and it's hard coded:
            PARAMETER(P0=101320.E0)
        Here in this python function it's an input parameter because that's
        conceptually clearer (the input includes all parameters that
        affect the result) and because it needs to have the same
        units as plid (which is an input parameter).
     
    Returns
    -------
    ap, b : numpy arrays
        Each is an array (of floats), same shape as input array 'lev'.
        These allow calculation of pressure on model levels according to:
            p = ap + b*ps
        where
            p = pressure on model level
            ps = instantaneous surface pressure
    '''
    coord = coord.strip() 
    if coord == 'ET15':
        # This is the case ICOORD.EQ.NC4TO8("ET15") in COORDAB
        power = 1.5
    else:
        raise Exception('Unknown AGCM vertical coordinate type: {}'.format(coord))
        
    plid = float(plid)
    pref = float(pref)
    assert plid < pref, 'AGCM reference lid pressure should be less than reference surface pressure'

    eta, p_T, p0 = lev, plid, pref # use notation of Beagley et al. 1997
    
    eta_T = p_T/p0 # in COORDAB this is the ETOIT parameter
    b = ( (eta - eta_T) / (1. - eta_T) )**power
    ap = p0*(eta - b)
    return ap, b

def get_lev_bnds(lev, lay, plid, pref, levb=None):
    '''
    Determine vertical level bounds, following the method used in subroutine BASCALX,
        https://gitlab.science.gc.ca/CanESM/CanAM/blob/develop_canesm/lssub/model/agcm/gcm18/modl/bascalx.f
    to determine the location of layer bases.

    Parameters
    ----------
    lev : numpy array
        Array (of floats) giving eta values of the model levels.
        Can be in order of ascending or descending altitude.
        Example: 
            lev = array([ 0.995, 0.986, ... ])
        In BASCALX this is the SG (or SH) input variable.
    lay : str
        Specifies the layering scheme.
        This parameter appears in the AGCM basefile:
                lay="    2"     ;
        In BASCALX this is the LAY input parameter and its meaning is
        described (en francais) in the comments there.
    plid : float or int
        Minimum pressure, a constant.
        Must be in same units as pref.
        See docstring in coordab() function for info on this parameter.
    pref : float or int
        Reference pressure, a constant.
        Must be in same units as plid.
        See docstring in coordab() function for info on this parameter.
    levb : numpy array
        Array (of floats) giving eta values.
        Required for layering schemes requiring both level bases
        and middles.
        In BASCALX this is the SGB (or SHB) input variable.

    Note, plid & pref are not used in BASCALX but are needed here to set
    the full lev_bnds array correctly.

    Returns
    -------
    lev_bnds : numpy array
        Array (of floats) giving eta values of the model level bounds.
        Array shape is (n,2) where n = len(lev).
        Example:
            lev_bnds = array([[ 1.        , 0.99048978 ],
                              [ 0.99048978, 0.97897497 ],
                              [ 0.97897497, 0.96497461]
                              ... ]])
        Note the repeated values: lev_bnds[m,1] == lev_bnds[m+1,0].
        This is the required format of bounds arrays for CMOR-ized
        netcdf files.
    '''
    assert is1D(lev), 'Input must be a 1D array'
    n = len(lev) # NOMBRE DE COUCHES

    plid = float(plid)
    pref = float(pref)
    p_T, p0 = plid, pref # use notation of Beagley et al. 1997
    eta_T = p_T/p0
    
    b = np.ones(n+1)
    b.fill(np.nan)
    if   np.all( np.diff(lev) < 0 ):
        # Levels are ordered from lowest altitude to highest altitude.
        # E.g. 0.995, 0.986, 0.972, ...
        b[0]  = 1.
        b[-1] = eta_T
    elif np.all( np.diff(lev) > 0 ):
        # Levels are ordered from highest altitude to lowest altitude.
        # E.g. 0.00102, 0.00265, 0.00612, ...
        b[0]  = eta_T
        b[-1] = 1.
    else:
        raise Exception('Inconsistent direction of model levels array')
    # Set level bounds following the logic in BASCALX based on the AGCM's
    # LAY parameter.
    if lay == 2:
        # Use geometric mean, as used in BASCALX to determine location of
        # layer bases.
        b[1:-1] = np.sqrt( lev[:-1]*lev[1:] )
    else:
        # Add other LAY values as needed.
        # If adding other cases, be aware that input array 'lev' can be
        # ordered in either direction (ascending or descending altitude).
        raise Exception('Unknown value of LAY parameter for AGCM: {}'.format(lay))
    lev_bnds = np.ones((n,2))
    lev_bnds.fill(np.nan)
    lev_bnds[:,0] = b[:-1]
    lev_bnds[:,1] = b[1:]
    assert not np.any(np.isnan(lev_bnds)), 'Unset values in lev_bnds' # ensure all values were explicity set
    return lev_bnds

###############################################################################
# Functions for displaying info
def show_field_info(dv, show_msg=True):
    '''Display info on the field contained in dict 'dv'.
    This is basically the contents of a ccc file, but having
    undergone preprocessing after being loaded by amor.py but
    before being prepared for CMOR-ization.'''
    indent = [' '*4*m for m in [1,2]]
    lw = []
    for s in sorted(dv.keys()):
        v = dv[s]
        if isinstance(v, np.ndarray):
            sv = s + ', array shape: ' + str(v.shape)
        elif isinstance(v, (str, unicode)):    
            sv = s + ': ' + v
        else:
            sv = s + ': ' + str(v)
        lw += [indent[1] + sv]
    if show_msg: msg('\n'.join(lw))
    else: return lw

def show_all_field_info(d_var):
    '''Loop over fields in d_var to show info on them.'''
    l_field = sorted( d_var.keys() )
    indent = [' '*4*m for m in [1,2]]
    lw = []
    lw += ['-'*50]
    lw += [ 'List of fields in ccc file: ' + ', '.join( l_field )]
    lw += [ 'Details of each field:']
    for field in l_field:
        dv = d_var[field]
        lw += [indent[0] + field]
        lw += show_field_info(dv, show_msg=False)
    lw += ['-'*50]
    msg('\n'.join(lw))
###############################################################################
# Function to load ccc files (but does not CMOR-ize them).
def load_ccc(
        cmortable       = None,
        cmorvar         = None,
        cccunits        = None,
        cccts           = None,
        chunk           = None,
        dimensions      = None,
        frequency       = None,
        optdeck         = None,
        user_f          = 'cccma_user_input.json',
        production      = False,
        one_field_per_input_file = True,
    ):
    '''
    Load ccc file (a data file in CCCma binary format).
    Makes some decisions about the meaning of the ccc file contents. 
    Since ccc files have almost no metadata, this requires bringing 
    in some external info.
    
    Parameters
    ----------
    
    
    Returns
    -------
    
    
    '''
    msg( '\n * starting load_ccc * ' )
    
    d_table = {}

    # It's possible for ccc files to contain more than one variable. 
    # Setting one_field_per_input_file=True means that only variable per ccc
    # file is allowed, and the function will fail if more than one is found.
    # This is useful because it can then be assumed that the one variable in
    # the file is the one being converted, whatever its name is, and thus it's
    # not necessary to know its name in order to find it in the ccc file.

    # Option to relabel model years. E.g. if we decided that we want year
    # 5000 of picontrol to appear as year 1 in the converted netcdf file.
    # Since we agreed (Jan 2019) NOT to do this for our CMIP6 output, 
    # relabel_model_years should be False for CMIP6 production.
    relabel_model_years = not True

    # Options for displaying info about the data and its conversion.
    show_ccc_file_info = True
    show_d_var_info = True    

    # Option to write only a subset of times to the netcdf output file.
    # This is only intended for testing purposes.
    # If subset_times = None then no subsetting is done.
    # To subset times, set subset_times = array of indices to retain from 
    # time grid
    subset_times = None    # retain all times in the input file
    #subset_times = np.arange(0,3)
    #subset_times = np.arange(1,4)  


    ###########################################################################
    if True: 
        # show input args & their values
        frame = inspect.currentframe()
        args, _, _, values = inspect.getargvalues(frame)
        print('function name "%s"' % inspect.getframeinfo(frame)[2])
        for i in args:
            print("    %s = %s (%s)" % \
                (i, values[i], values[i].__class__.__name__))
    ###########################################################################
 
    # Get info from the "dataset" json file.
    # This includes CCCma-specific information including model information such
    # as the model name, its calendar type, etc.
    with open(user_f, 'r') as f:
        d_user_f = json.load(f)

    # Get variable info from json tables
    d_table[cmortable] = get_table_info(cmortable)
 
    #show_cmorvar_json_info(cmortable, cmorvar)

    if VALIDATE_CMORVAR_INFO:
        # Validate the dimensions passed as input against the requested
        # dimensions indicated in the json table
        s = d_table[cmortable]['variable_entry'][cmorvar]['dimensions']
        if dimensions != s:
            # override input dimensions with requested ones
            lw = []
            lw += ['-'*50]
            lw += ['Dimensions mismatch!']
            lw += ['    input dimensions:     ' + dimensions]
            lw += ['    requested dimensions: ' + s]
            lw += ['Overriding input dimensions with requested ones']
            lw += ['-'*50]
            msg('\n'.join(lw))
            dimensions = s
            
        s = d_table[cmortable]['variable_entry'][cmorvar]['frequency']
        if frequency != s:
            msg('Frequency mismatch. Overriding input (' + frequency \
                + ') with requested (' + s + ').')
            frequency = s 

    # Get coordinates info from json tables
    d_table['CMIP6_coordinate'] = \
        get_table_info('CMIP6_coordinate')
        
    # Get list of all possible requested altitude grids.
    z_grids = get_requested_z_grids( d_table['CMIP6_coordinate'] )
    
    # Get info (levels, units) about requested level sets.
    # This info is needed to:
    #  1) check that the requested levels are all present 
    #  2) subset the ccc file data to retain only the requested levels
    d_levs_req = get_requested_levels( d_table['CMIP6_coordinate'] )
    # d_levs_req now contains the requested levels for those level sets that
    # have specific requested levels. For level types that don't have specific
    # requested levels (e.g. model levels), d_lev_req will still have an entry,
    # but its 'levels' array will be empty.
    
    # Identify the model. This is used to define model characteristics such as
    # spatial resolution and timestep. This is the source_id parameter in the
    # CMIP6 json files.
    model = d_user_f['source_id'] # e.g. 'CanESM5'

    if not production:
        # A non-official run can have a source_id like CanESM5-${runid}.
        # Assume that the model characteristics are those of CanESM5
        # (or CanESM5-CanOE, although this doesn't matter since for the
        # AGCM CanESM5-CanOE is the same as CanESM5).
        if model.startswith('CanESM5-CanOE-'):
            model = 'CanESM5-CanOE'
        elif model.startswith('CanESM5-1'):
            model = 'CanESM5-1'
        else:
            model = 'CanESM5'
        msg( 'For non-official run, assuming model = ' + model )
    assert model in D_MODEL, 'Unknown model: ' + model

    d_levs_req2out = D_MODEL[model]['resolution']['d_levs_req2out']
    # d_levs_req2out maps from requested levels to levels used in the output.
    # For pressure levels (plev19, etc) the requested and output level set are
    # the same thing.
    # For model levels, the data request specifies model levels as 'alevel'
    # or 'alevhalf', but since different models have different types of levels
    # we need to convert this to the specific levels type used in our model.
    # This levels type must be one of the types allowed in the CMIP6 json file
    # (CMIP6_coordinate.json).
    
    for dn in MODEL_LEVELS_REQ: 
        assert dn in d_levs_req2out
    model_levels_used = [d_levs_req2out[dn] for dn in d_levs_req2out]
    d_levs_out2req = { d_levs_req2out[s] : s for s in d_levs_req2out}
    assert len(d_levs_out2req) == len(d_levs_req2out)

    # Load ccc input file (ccc = CCCma binary format)
    dirpath = '.'
    filename = cccts
    filepath = os.path.join(dirpath, filename)
    ###########################################################################
    # Copied from /usr/local/cview2015/cccma/filereader.py:

    #09/20/2010 - This variable controls whether superlabels should always override the regular field name.
    #          It is passed on to the binary file reader when the file is opened.
    #          When disabled, the original heuristics (from the 2008 IDL ccidv) are used.
    #          This MUST be left disabled in the CCCma_FileReader case for backward compatibility.
    #          If there's a need to enable it, create a subclass (see CCCma_FileReaderSuperlabels below)
    always_use_superlabels = False
    
    #10/30/2012 - The sort_vars property governs whether the set of records is sorted before
    #             being exposed to the python interface. With this enabled, the set of
    #             records with the same variable name is conglomerated and sorted by level and
    #             timestep. This is generally desirable behavior, but we provide the option
    #             to turn it off if the authors of individual files use the record order to
    #             store out of band information.
    sort_vars = True
    ###########################################################################
    assert cccma_py, 'Need to import cccma_py module to load ccc format files'
    msg('Using cccma_py version: ' + cccma_py.__file__)

    # For some variables we don't want sort_vars=True.
    #
    # * Note for future development * (19 Aug 2019)
    # Possibly we could set sort_vars=False for all variables. This might be
    # simpler. The initial reason to use sort_vars=True was that for ccc files
    # with multiple variables it produced simpler output. However pycmor
    # currently only handles cases of a single variable per ccc file (we
    # always have one_field_per_input_file=True), so this doesn't matter.
    # It would also mean we don't have to rearrange any mis-sorted levels (see
    # comments below), simplifying the code. With sort_vars=False all the time,
    # there'd be no need for logic here to identify variables for which
    # sort_vars=True, so that all this logic could be at one place, below,
    # where dimensions & grids are altered if needed (expand_dim & set_grid).
    lb = []
    lb.append(      dimensions  == 'longitude latitude plev7c tau time' \
                and optdeck     == 'cospmask_prestau_v2' )
    lb.append(      dimensions  == 'longitude latitude effectRadIc tau time' \
                and optdeck     == 'cospmask_refftau' )
    lb.append(      dimensions  == 'longitude latitude effectRadLi tau time' \
                and optdeck     == 'cospmask_refftau' )
    lb.append(      dimensions  == 'longitude latitude alt16 tau time' \
                and optdeck     == 'cospmask_misr_alt_tau' )

    # placeholders (3sep.19) - no model output for these vars yet
    lb.append(      dimensions  == 'longitude latitude alt40 dbze time' \
                and optdeck     == 'PLACEHOLDER' )  # will be for cfadDbze94_Emon
    lb.append(      dimensions  == 'longitude latitude alt40 scatratio time' \
                and optdeck     == 'PLACEHOLDER' ) # will be cfadLidarsr532_Emon


    # template for a new case
    #lb.append(      dimensions  == '' \
    #            and optdeck     == '' )

    if any(lb):
        sort_vars=False
        msg('Setting sort_vars=False for variable {}_{} with dimensions=\'{}\''\
            .format(cmorvar, cmortable, dimensions))

    # Open ccc file ("ccc" = CCCma binary format)
    f = cccma_py.open(filepath, always_use_superlabels, sort_vars)
    if f is None:
        msg('Failed to open ccc time series file: ' + filepath, excep=True)
    msg('Opened file: ' + filepath + '  (' + file_size_str(filepath) + ')')
    lv = f.getvarlist()
    del f
    
    ###########################################################################
    if show_ccc_file_info:
        # Display info about the variables found in the ccc file.
        # This gives a basic view of the file content, before any manipulation
        # is done on it.
        nv_max = 5
        nv_tot = len(lv)
        nv = 1
        lw = []
        lw += ['='*50]
        lw += ['Showing info on variables found in ccc input file']
        for v in lv:
            lw += ['-'*50]
            for att in ['varname', 'var_index', 'timesteps', 'levels']:
                if not hasattr(v, att): continue
                ad = getattr(v, att)
                if att in ['timesteps', 'levels']:
                    n = len(ad)
                    m_max = 10
                    s = ''
                    if len(ad) > m_max:
                        ad = ad[:m_max]
                        s = ', ...'
                    lw += [att + ': ' + ', '.join([str(c) for c in ad]) + s]
                    lw += ['no. of ' + att + ': ' + str(n)]
                else:
                    lw += [att + ': ' + str(ad)]
            ibuf = v.load_ibuf(0,0)    
            # v.load_ibuf() seems only to work for input args 0,0, but that's
            # fine I guess. I don't see why we'd need info from other IBUFs.
            lw += ['IBUF: ' + str(ibuf)]
            lw += ['data (timestep, level, array shape, min, max):']
            n = 0
            n_tot = len(v.timesteps)*len(v.levels)

            # n_max = max number of horizontal fields for which to show info
            #n_max = 40 
            #n_max = len(v.levels)
            n_max = 11

            for k, t in enumerate(v.timesteps):
                for m, lev in enumerate(v.levels):
                    ad = v.load_field(k,m) 
                    # If the field doesn't exist for this k,m then
                    # v.load_field() returns None.
                    # If it does exist then 'ad' should be a numpy array
                    # containing the field at this timestep and level.
                    # The dimensions of the array are lat x lon.
                    ta = ( t, lev, str(ad.shape), ad.min(), ad.max() )
                    lw += [' '*4 + '  '.join(str('%-8s' % s) for s in ta)]
                    n += 1
                    if n >= n_max: break
                if n >= n_max: break
            lw += ['total no. of horizontal fields available: ' + str(n_tot)]
            lw += ['-'*50]
            nv += 1
            if nv > nv_max: break
        lw += ['total no. of variables available: ' + str(nv_tot)]
        lw += ['='*50]
        msg('\n'.join(lw))

    ###########################################################################
    # Create a dict holding all the variables in the file.
    l_varname = [v.varname for v in lv]
    assert chksme(l_varname) == l_varname, \
        'variable name should uniquely identify each variable'
        
    if one_field_per_input_file:
        # Ensure that assumption of one variable per file is correct.
        assert len(lv) == 1
 
    calendar = str(d_user_f['calendar'])

    td_units = 'days'

    # Specify the time axis origin, aka reference date.
    # This involves no relabelling of model time as it appears in the original
    # model output files. It only affects the numerical value of the monotonic
    # time grid. When CMOR is passed the correct units of the time grid,
    # specified here by ref_date_str, it automatically converts the monotonic
    # time grid values into a date range and uses that in the time string
    # appearing in the netcdf file name.
    get_ref_date_from_static_table = True
    if get_ref_date_from_static_table:
        # get ref date from static table (the user_f file)
        ref_date = str(d_user_f['#time_axis_origin'])
        # ref_date should be a string of the form 'days since 1850-01-01 0:0:0.0'.
        # parse this string to make dict d_ref giving the date
        s0 = 'days since '
        assert ref_date.startswith(s0)
        ymd, hms = ref_date.strip(s0).split()
        ymd = ymd.split('-')
        ymd = [int(s) for s in ymd]
        hms = hms.split(':')
        hms = [int(s) for s in hms[:2]] + [float(s) for s in hms[2:]]
        l = ['year', 'month', 'day', 'hour', 'minute', 'second']
        assert l == mt.date_order
        ymdhms = ymd + hms
        d_ref = {s : ymdhms[m] for m,s in enumerate(l)}
        ref_date0 = ref_date
    else:
        # 13 Feb 2019: it was decided the ref date will be specific in the
        # static table. So don't allow it to be specified here.
        raise Exception('Must get time axis origin from static table')
        #d_ref = {'year' : 1850, 'month' : 1, 'day' : 1}
        
    # Make string indicating the reference date.
    # E.g. 'days since 1850-01-01 12:0:0.0'.
    # It's formatted in the same way as the cdtime module uses when it prints
    # a date, since CMOR seems to understand this format.
    # According to the CMOR manual, cdtime is what CMOR uses internally to
    # convert between date and monotonic time.)
    #time_output = 'seconds'
    time_output = 'days'
    mt.date_check(d_ref)
    ref_date = str('%.4i-%.2i-%.2i %i:%i:%.1f' % \
        tuple([d_ref[s] for s in mt.date_order]))
    ref_date_str = time_output + ' since ' + ref_date
    
    if get_ref_date_from_static_table:
        # ensure that ref date in static table was decoded correctly by making
        # sure we get the same string back
        assert ref_date_str == ref_date0, 'ref date string mismatch: ' \
            + ref_date_str + ', ' + ref_date0

    msg( 'Using reference date: ' + ref_date )
    
    if relabel_model_years:
        # model_year_offset = the model year that will be treated as year zero.
        # e.g. if the model ran 5000 years but we want to label year 4000 as
        # year zero, set model_year_offset = 4000.
        model_year_offset = 5200

    ###########################################################################    
    # Loop over variables in the file. 
    # The variables are stored in dict d_var. 
    d_var = {}
    for v in lv:
        ibuf = v.load_ibuf(0,0)
        ad = v.load_field(0,0)
        
        if one_field_per_input_file:
            # Here make adjustments to ibuf['KIND'], if necessary, to identify
            # the type of data in the ccc file. ibuf['KIND'] is a label from
            # the ccc file indicating the type of data stored in the file, but
            # unfortunately there are cases for which the given label is misleading.
            
            # For data at sites, the ccc files have no way to indicate this.
            # It would be useful if ibuf['KIND'] indicated this, but instead
            # it says 'GRID', indicating a lat-lon grid.
            # To pretend like ibuf indicated the nature of the array dimension,
            # set ibuf here based on the dimensions string.
            dims_ip = dimensions.split()
            if 'site' in dims_ip:
                ibuf['KIND'] = 'SITE'
            else:
                # ensure the same string is not used for some other case
                assert ibuf['KIND'] != 'SITE', 'Invalid use of ibuf[KIND]=SITE'
                
            # Data that are a single time series (i.e. not an array) can have
            # ibuf['KIND'] == 'GRID' even though it's not a 2D horizontal field.
            # (This was discovered by trying to convert co2mass_Amon.)
            # Here check the ILG and ILAT parameters in ibuf to identify this
            # case and set ibuf['KIND'] to identify it.
            if ibuf['ILG'] == 1 and ibuf['ILAT'] == 1:
                ibuf['KIND'] = 'POINT'
            else:
                # ensure the same string is not used for some other case
                assert ibuf['KIND'] != 'POINT', 'Invalid use of ibuf[KIND]=POINT'
                
        # ibuf['KIND']: a rare example of metadata in ccc files
        if ibuf['KIND'] in ['GRID']: 
            # 2D horizontal field
            assert len(ad.shape) == 2, \
                'Unexpected array shape for horizontal field: {}'.format(ad.shape)
            nlat = ad.shape[0]
            nlon = ad.shape[1]
        elif ibuf['KIND'] in ['ZONL']: 
            # zonal-mean field (i.e. average over longitude)
            assert len(ad.shape) == 1, \
                'Unexpected array shape for zonal-mean field: {}'.format(ad.shape)
            nlat = ad.shape[0]
            nlon = 1
        elif ibuf['KIND'] in ['SITE']:
            # a single array dimension, representing different sites
            assert len(ad.shape) == 1, \
                'Unexpected array shape for sites array: {}'.format(ad.shape)
            nlat = ad.shape[0]
            nlon = 1
        elif ibuf['KIND'] in ['POINT']:
            # a single number, not an array (e.g. global mean)
            assert ad.shape == (1,), \
                'Unexpected array shape for point array: {}'.format(ad.shape)
            nlat = 1
            nlon = 1
        else:
            raise Exception(\
                'Unknown case for horizontal dimensions: IBUF[\'KIND\'] = ' \
                + ibuf['KIND'])
        nlev = len(v.levels)
        ccc_times = np.array(v.timesteps) # array of the times as given in the ccc input file
        ntim = len(ccc_times)
        load_times = np.arange(ntim)

        if subset_times is not None:
            print('\n\n * * * WARNING: using subset_times to truncate the times. This is for TESTING ONLY. * * * \n\n')
            assert type(subset_times) == np.ndarray
            load_times = subset_times
            ccc_times = ccc_times[load_times]
            ntim = len(ccc_times)
        
        # Store data as array with dimensions in the order:
        #   latitude, longitude, altitude, time
        shp = (nlat, nlon, nlev, ntim) 
        # Make list indicating what these dimensions are. They are always the same
        # in ccc files. This list isn't used within this function but is handy to
        # pass out to the calling function so that the user can know what are the
        # dimensions of the data.
        ccc_dims = ['latitude', 'longitude', 'level', 'time']
        
        # Initialize data array as an array of NaN (Not a Number) values
        ad = np.zeros(shp)
        ad.fill(np.nan)
        
        # Loop over times & levels to fill the array
        for m in range(nlev):
            for k,t in enumerate(load_times):
                #b = v.load_field(k,m) 
                b = v.load_field(t,m) 
                if len(b.shape) == 1: b.shape = (b.shape[0], 1)
                assert b.shape == (nlat, nlon), 'Wrong shape for lat,lon array'
                ad[:,:,m,k] = b

        # Ensure all the nan values were overwritten with data from the file
        assert not any(np.isnan( ad.flatten() ))
        dv = {
            'field'         : ad
        ,   'ibuf'          : ibuf
        ,   'latitude'      : cccma_py.get_lats(nlat)
        ,   'level'         : np.array(v.levels)
        ,   'time'          : np.array(ccc_times)
        }
        if   ibuf['KIND'] in ['GRID', 'ZONL']:
            # Ensure latitude grid is an array.
            # cccma_py.get_lats() will only return an array of latitudes if
            # a valid nlat is passed to it.
            assert isinstance(dv['latitude'], np.ndarray), \
                'Latitude grid must be an array'
            
            if nlon > 1:
                # Set longitude grid.
                # Assume longitudes run from 0 to 360 deg on an evenly spaced grid
                # with nlon points. 
                dv['longitude'] = np.linspace(0, 360., nlon)
                # Remove the redundant longitude that is stored in ccc files
                dv['field'] = dv['field'][:,:-1,:,:]
                dv['longitude'] = dv['longitude'][:-1]
                nlon = len(dv['longitude'])
            else:
                dv['longitude'] = np.array([0.])

        elif ibuf['KIND'] in ['SITE', 'POINT']:
            # No lat or lon grids in this case.
            dv['longitude'] = None
            # Note, dv['latitude'] was already set to None above.
            
        else:
            raise Exception(\
                'Unknown case for horizontal dimensions: IBUF[\'KIND\'] = ' \
                + ibuf['KIND'])

        #######################################################################
        # Vertical levels
            
        set_level_units = True
        decode_levels = True
        
        if set_level_units:
            # Find what is the requested vertical dimension and use it to
            # determine the units of the levels (e.g hPa for pressure levels).
            # Ideally the ccc file would indicate the units of its vertical
            # grid. But ccc files don't contain this info, they just give
            # numbers for the vertical levels without indicating what is the
            # grid type.
            # To get around this problem, check here what are the requested
            # levels. Then assume that whatever is found in the ccc file
            # is on these levels.
        
            dims_req = dimensions.split()
            l = [dn for dn in dims_req if dn in z_grids]
            
            if len(l) == 0:
                # The requested variable has no vertical dimension specified
                level_units = None
                level_type = 'no requested levels'
            elif len(l) == 1:
                # The requested variable has a vertical dimension
                dn = l[0]
                if dn in d_levs_req2out: dn = d_levs_req2out[dn]
                d_coord = d_table['CMIP6_coordinate']['axis_entry']
                assert dn in d_coord, 'Grid not found in json table: ' + dn
                level_units = d_coord[dn]['units'] 
                # level_units now contains the units of the requested levels
                if level_units in ['Pa']:
                    # If pressure in Pa is requested, assume that ccc file
                    # gives pressure in hPa
                    level_units = 'hPa'
                level_type = dn
            else:
                # More than one vertical dimension was found in dims_req.
                # This should not happen!
                raise Exception('More than one vertical dimension was found: '\
                    + ', '.join(l))
            dv.update({'level_units' : level_units, 'level_type' : level_type})

        if decode_levels:
            # Assume that any negative values in the 'level' array are CCCma
            # level encoding for pressure levels above 10 hPa. 
            # The v.levels array from cccma_py seems to be in the wrong order
            # for these levels, so reorder it here. That is, it's sorted 
            # according to the integer values, but we want it sorted by
            # altitude. 
            # The reason it's in the wrong order is due to setting 
            #   sort_vars=True
            # in cccma_py.open(). Setting it False puts the levels back in
            # order. However, setting it False also causes some variable names
            # to be repeated in l_varname. Not sure why this is, and I can't
            # see under the hood in cccma_py. So for now I'll leave
            # sort_vars=True and rearrange levels here as needed.
            # (JA, 17 Dec 2018)

            if any(dv['level'] < 0):
                # Decode vertical levels from CCCma format (integer) into float
                g1 = dv['level'] 
                # g1 = integers specifying the levels found in the ccc file
                #nlev = len(g1)   # no. of levels
                g2 = np.array( [ decode_cccma_level(m) for m in g1 ] ) 
                # g2 = the level integers decoded into floats
                assert all(g2 == np.array( chksme(g2) )), \
                    'levels must be unique'
                # g2 now contains the levels as floats, in the order as was
                # loaded from the ccc file. The is the current ordering of
                # levels in the data array. 
                # Now order the levels from highest altitude to lowest altitude.
                lt = zip(g2,g1,range(nlev)) 
                # lt = list of tuples specifying levels in the order in the
                # data array
                lt = sorted(lt) 
                # lt is now sorted by the float values of the levels (g2)
                ind = [t[2] for t in lt] 
                # The last entry of the tuple is the index of the level in the
                # data array, so 'ind' now indexes levels in the data array.
                assert ind == chksme(ind), 'indices cannot be repeated'
                assert len(ind) == nlev, 'levels are missing from index list'
                ad = dv['field'] # ad = data array for the current variable
                b = np.zeros(ad.shape)
                b.fill(np.nan)
                g = np.zeros(nlev)
                g.fill(np.nan)
                # Loop over ind to put the levels in the correct order
                for k,m in enumerate(ind):
                    b[:,:,k,:] = ad[:,:,m,:]
                    g[k] = lt[k][0]
                del ad
                # Ensure all the nan values were overwritten with data
                assert not any( np.isnan( b.flatten() ) )    
                assert not any( np.isnan( g ) )
                # Ensure levels are ordered from highest altitude to lowest
                # altitude. Since the levels are given as 1000*eta, which
                # corresponds roughly to their pressure in hPa, smallest
                # numbers are highest altitudes. 
                assert all( np.diff(g) > 0 )
                # Replace data array and vertical grid with the correctly
                # ordered ones
                dv['field'] = b
                dv['level'] = g

        #######################################################################        
        # Check that spatial dimensions are as expected    
        d_expected = D_MODEL[model]['resolution']
        if   ibuf['KIND'] in ['GRID', 'ZONL']:
            nlat0 = d_expected['nlat']
            nlon0 = d_expected['nlon']
            if ibuf['KIND'] in ['ZONL']: nlon0 = 1 # zonal-mean data
            # Check horizontal grids
            assert nlat == nlat0, 'Unexpected size of latitude dimension: ' \
                + str(nlat) + ' (' + str(nlat0) + ' expected)'
            assert nlon == nlon0, 'Unexpected size of longitude dimension: ' \
                + str(nlon) + ' (' + str(nlon0) + ' expected)'
        # Check number of model levels
        # (Are model levels data always the full set of model levels or might
        # they be truncated?)
        if 'level_type' in dv:
            dn = dv['level_type']
            if dn in model_levels_used:
                # convert name of specific model level (e.g. 
                # 'alternate_hybrid_sigma') to name of requested model level
                # (e.g. 'alevel')
                dn = d_levs_out2req[dn] 
                if dn in d_expected:
                    # Check that model levels are as expected
                    lev_str = d_expected[dn]
                    levs0 = parse_levels_string(lev_str)
                    if decode_levels:
                        # decode the integers into floats
                        levs0 = np.array([decode_cccma_level(m) for m in levs0])
                    # levs0 is now an array containing the expected levels
                    assert len(levs0) == len(dv['level']), \
                        'Unexpected number of model levels'
                    if not np.allclose(levs0, dv['level']):
                        msg('For model "{}", expected levels are:'.format(model))
                        msg(str( levs0 ))
                        msg('Levels actually found in the model output file do not match these:')
                        msg(str( dv['level'] ))
                        raise Exception('Unexpected values for model levels')

        #######################################################################
        # Time grid
        
        convert_time_units = True
        
        if frequency in ['fx']:
            convert_time_units = False

        if convert_time_units:
            # Convert units of the time array given by the ccc file.
            
            tv = dv['time']  
            # tv = array giving time grid as provided in the ccc file
            assert isinstance(tv, np.ndarray), 'ccc file time grid should be an array'
            assert isinstance(tv[0], int), 'ccc file time grid should be array of integers'
            
            time_bnds_method = 'requested frequency'

            time_input = None 
            # time_input : string describing what kind of info (date,
            # model timestep) is provided in the ccc file.
            # Unfortunately the ccc file itself doesn't specify what this is,
            # and (also unfortunately) it isn't the same in all ccc files.
            # 
            # Here set time_input based on the temporal frequency of the
            # requested variable. This assumes that ccc files of a given
            # frequency all have the same format of time grids.
            #
            # Valid 'frequency' attribute values in the data request:
            #   1hr, 1hrCM, 1hrPt, 3hr, 3hrPt, 6hr, 6hrPt, day, dec, fx,
            #   mon, monC, monPt, subhrPt, yr, yrPt
            if frequency in ['yr', 'yrPt']:
                time_input = 'YYYY'
            elif frequency in ['mon', 'monPt', 'monC']:
                time_input = 'YYYYMM'
            elif frequency in ['day']:
                time_input = 'YYYYMMDD'
            elif frequency in ['6hr', '6hrPt']:
                time_input = 'YYYYMMDDHH'
            elif frequency in ['3hr', '3hrPt', '1hr', '1hrPt', '1hrCM', 'subhrPt']:
                time_input = 'model timestep'
            else:
                msg('Unknown frequency: ' + frequency, excep=True)
        
            date_formats = ['YYYY', 'YYYYMM', 'YYYYMMDD', 'YYYYMMDDHH']

            validate_time_ranges = True
            check_date_parsing = True

            dpm = mt.get_dpm(calendar) # no. of days per month
            mpy = len(dpm) # no. of months in the year
            spd = mt.conversion_factors['seconds per day']
            hpd = mt.conversion_factors['hours per day']
            
            delt = D_MODEL[model]['resolution']['delt']
            
            # Get ranges of valid years and timesteps to check against the
            # results of parsing the given times.
            # yr0 = start year, yr1 = end year
            assert isinstance(chunk, str)

            # Determine chunk format based on length of the chunk string
            valid_chunk_formats = ['YYYY_YYYY', 'YYYYMM_YYYYMM']
            l = [len(s) for s in valid_chunk_formats]
            assert chksme(l) == l, 'valid chunk format strings must have unique lengths'
            for chunk_format in valid_chunk_formats:
                if len(chunk) == len(chunk_format):
                    break

            if   chunk_format in ['YYYY_YYYY']:
                yr0, yr1 = tuple( [int(y) for y in chunk.split('_')] )
                mon0 = 1
                mon1 = 12
            elif chunk_format in ['YYYYMM_YYYYMM']:
                l = chunk.split('_')
                m = 4
                yr0, yr1 = tuple([int(s[:m]) for s in l])
                mon0, mon1 = tuple([int(s[m:]) for s in l])
            else:
                raise Exception('Unknown time chunk format: ' + chunk_format)
            d_model_start = D_MODEL[model]['model_start']

            # d_model_start = date at which the model starts running. This is
            # a property of the model!
            mt.date_check(d_model_start)
            tdms0 = mt.date2time({'year' : yr0, 'month' : mon0,  'day' : 1 }, \
                d_model_start, calendar, t_units=td_units)
            tdms1 = mt.date2time({'year' : yr1, 'month' : mon1, 'day' : 31}, \
                d_model_start, calendar, t_units=td_units) + 1
            # tdms0, tdms1 = number of days since model started that fall
            # within the range given by yr0, yr1.
            # Use tdms0, tdms1 to work out what range of model timesteps
            # covers the time chunk.
            tspd = spd/float(delt) # no. of model timesteps per day
            ts0 = tdms0*tspd
            ts1 = tdms1*tspd


            # Is the field accumulated?
            how_determine_accumulated = 'time of day'
            how_determine_accumulated = 'data request'
            cell_methods = d_table[cmortable]['variable_entry'][cmorvar]['cell_methods']
            if how_determine_accumulated == 'time of day':
                ###########################################################
                # 20 Sep 2019 update: this method CANNOT be used. It used
                # to indicate whether a field was sampled or accumulated,
                # but Jason and Mike modified things so that this is no
                # longer true. It can't be relied on.
                raise Exception('Invalid method for determining sampled vs. accumulated')
                ###########################################################
                
                # Assume that a sampled field will be available at hours
                # starting with 00Z (midnight). If the first time is not
                # 00Z but instead is the sampling period, e.g. 03Z (3 AM)
                # then assume the field is accumulated. This is because
                # accumulated fields are output with the timestep
                # indicating the end of the accumulation period.
                # For example: if the sampling period is 3 hours, then
                # sampled output should be available at these times of day:
                #   00, 03, 06, 09, 12, 15, 18, 21
                # whereas accumulated output should be available at these
                # times of day:
                #   03, 06, 09, 12, 15, 18, 21, 24
                # As long as the file contains at least one full day, and
                # starts at the beginning of that day (this is normally
                # the case) then we can look at the first timestep in the
                # file and see whether it's at 0Z or not.
                assert tdms1 - tdms0 >= 1, 'Input ccc file must contain at least one whole day'
                assert np.mod(spd, delt) == 0, 'Model timestep ({} s) must divide evenly into a day ({} s)'\
                    .format(delt, spd)
                # tv[0] = first timestep in the ccc input file
                # tspd = number of timesteps per model day
                accumulated = np.mod(tv[0], tspd) != 0
            
                if VALIDATE_CMORVAR_INFO:
                    # The above determines if the variable in the ccc input
                    # file is accumulated.
                    # Next question: should it be accumulated? Check what is
                    # requested by looking at the info from the json table.
                    if 'time: point' in cell_methods:
                        # Variable should be sampled
                        assert not accumulated, 'Variable {} (table: {}) should be sampled, not accumulated'\
                            .format(cmorvar, cmortable)
                    elif 'time: mean' in cell_methods:
                        # Variable should be accumulated
                        assert accumulated, 'Variable {} (table: {}) should be accumulated, not sampled'\
                            .format(cmorvar, cmortable)
                    else:
                        # Ambiguous. Need to decide what to do.
                        raise Exception('Unknown case: cell_methods={}'.format(cell_methods))
       
            elif how_determine_accumulated == 'data request':
                # Just assume that the variable in the ccc file is correct.
                if 'time: point' in cell_methods:
                    # Variable is sampled
                    accumulated = False
                elif   'time: mean'     in cell_methods \
                    or 'time: minimum'  in cell_methods \
                    or 'time: maximum'  in cell_methods \
                    or 'time: sum'      in cell_methods:
                    # Variable is accumulated
                    accumulated = True
                else:
                    # Ambiguous. Need to decide what to do.
                    raise Exception('Unknown case: cell_methods={}'.format(cell_methods))
                
            else:
                raise Exception('How to determine whether or not file contains accumulated or sampled data?')


            
            if time_input in date_formats:

                # Parse the time values in array 'tv' to get component of the date.
                # E.g. for time_input = 'YYYYMMDD' and tv[0] = 19840512, tv[0] is parsed to get
                # the year (1984), month (05) and day (12).
                # The first component can have any number of places.
                # E.g. for time_input = 'YYYYMMDD', the year doesn't have to be 4 digits.
                # It can be 1 digit, 4 digits, 10 digits or whatever.
                # The subsequent components do need to have the specified number of places.
                # So e.g. for time_input = 'YYYYMMDD', the month and day both have to take up two places.

                l_date_part = chksme(time_input) # list with one character for each part of the date, e.g. ['Y', 'M']
                d_date_part = {} # dict that will contain an array of years, months, etc
                d_fmt_date = {}
                timestamps = np.array(tv)
                for s in l_date_part[::-1]:
                    m0 = time_input.index(s)
                    m1 = time_input.rindex(s) + 1
                    nm = m1-m0 # number of places the date part occupies. E.g. for 'YYYYMM', s = 'M' gives nm = 2.
                    assert time_input[m0:m1] == s*nm, 'error processing {} part of {} timestamp'.format(s,time_input)
                    assert time_input.count(s) == nm, 'error processing {} part of {} timestamp'.format(s,time_input)
                    d_fmt_date[s] = '%.' + str(nm) + 'i'
                    if s in l_date_part[1:]:
                        # Extract the part of the integer giving the desired part of the date.
                        # Do this by getting the remainder after division by factor 'b' and then
                        # dividing by 'b' to prepare for the next loop iteration.
                        b = 10**nm
                        d_date_part[s] = np.mod(timestamps, b)
                        timestamps -= d_date_part[s]
                        timestamps /= b
                    else:
                        # If this is the last remaining part of the date, all the other parts have
                        # already been extracted and nothing else needs to be done.
                        d_date_part[s] = timestamps
                fmt_date = ''.join([d_fmt_date[s] for s in l_date_part])
                # d_date_part now contains an array for each part of the date. E.g. for year & month:
                #   d_date_part['Y'] = np.array([ 1980, 1980, 1980, ... ])
                #   d_date_part['M'] = np.array([    1,    2,    3, ... ])
                del timestamps, d_fmt_date

                if check_date_parsing:
                    fmt = '%.{}i'.format(len(time_input))
                    l_formatted_times = [str(fmt % m) for m in tv]
                    
                if validate_time_ranges:
                    # Check that valid values were found.
                    if 'Y' in d_date_part: # years
                        assert np.all( d_date_part['Y'] >= yr0 )
                        assert np.all( d_date_part['Y'] <= yr1 )
                    if 'M' in d_date_part: # months
                        assert np.all( d_date_part['M'] <= mpy )
                    if 'D' in d_date_part: # days
                        assert np.all( d_date_part['D'] >= 1 )
                        assert np.all( d_date_part['D'] <= max(dpm) )
                    if 'H' in d_date_part: # hours
                        assert np.all( d_date_part['H'] >= 0 )
                        assert np.all( d_date_part['H'] <= hpd-1 )
                    
                if relabel_model_years: 
                    d_date_part['Y'] -= model_year_offset
                
                # Get list of tuples giving the dates in the time grid. 
                # Each tuple entry is a part of the date, i.e. year, month,
                # day, or hour.
                # E.g. for YYYYMM format, the tuples are (year, month). 
                lt = zip( *[ d_date_part[sp] for sp in l_date_part ] )

                if check_date_parsing:
                    # Recreate the list of times to ensure that the values have
                    # been correctly parsed
                    d_t2 = {}
                    for sp in l_date_part:
                        g = np.array(d_date_part[sp])
                        if sp in ['Y'] and relabel_model_years: 
                            # for the check, undo the year relabelling
                            g += model_year_offset
                        d_t2[sp] = g
                    lt2 = zip( *[ d_t2[sp] for sp in l_date_part ] )
                    l2 = [str(fmt_date % t) for t in lt2]
                    assert l_formatted_times == l2, 'Time values incorrectly parsed'
                    
                d_dp = {'Y' : 'year', 'M' : 'month', 'D' : 'day', 'H' : 'hour'}
                
                t = lt[0]
                ld = [ { d_dp[sp] : t[k] for k,sp in enumerate(l_date_part) } 
                        for t in lt ]
                # Compute total time (i.e. time elapsed since the reference
                # date) for the dates in ld
                td = np.array( [mt.date2time(d, d_ref, calendar, t_units=td_units) for d in ld ] )
                    
                assert np.all( np.diff(td) > 0 ), 'Times should be monotonically increasing'
                    
                # Create time bounds array
                #nt = len(td)
                assert len(td) == ntim
                td_first = np.array(td)
                
                if time_bnds_method in ['requested frequency']:
                    # Set the time bounds based on the requested frequency.

                    if frequency in ['yr', 'yrPt']:
                        td_gap = np.array([ mt.get_dpy(calendar, d['year'])
                                            for d in ld ])
                    elif frequency in ['mon', 'monPt', 'monC']:
                        td_gap = np.array([ dpm[ int(d['month']-1) ]
                                            for d in ld ])
                    elif frequency in ['day']:
                        td_gap = np.ones(ntim)
                    elif frequency in ['6hr', '6hrPt']:
                        td_gap = np.ones(ntim)/(hpd/6.)
                    elif frequency in ['3hr', '3hrPt']: 
                        td_gap = np.ones(ntim)/(hpd/3.)
                        # I think we never end up here because model output
                        # at frequency higher than 6 hrs has model timestep
                        # as the time grid in the ccc file.
                        raise Exception('does frequency={} ever go here?'\
                            .format(frequency))
                    elif frequency in ['1hr', '1hrPt', '1hrCM']: 
                        td_gap = np.ones(ntim)/hpd
                        # I think we never end up here because model output
                        # at frequency higher than 6 hrs has model timestep
                        # as the time grid in the ccc file.
                        raise Exception('does frequency={} ever go here?'\
                            .format(frequency))
                    else:
                        raise Exception('Unknown frequency: {}'.format(frequency))
                
                elif time_bnds_method in ['highest frequency']:
                    # Set time bounds based on what is the highest frequency
                    # appearing in the time grid. 
                    #
                    # Problem with this method: the highest frequency
                    # specified by the date might not indicate the granularity
                    # of the time. 
                    # For YYYYMM values, could have bimonthly data:
                    #   185001, 185003, ...
                    # For YYYYMMDD values, could have monthly data:
                    #   18500101, 18500201, ...
                    # Instead of using this method, I'm going to let the
                    # requested frequency determine the time bound.
                    # This isn't great, but is consistent with how levels are
                    # treated. 
                    # (JA, Jan 2019)

                    sp = d_dp[ l_date_part[-1] ] 
                    # sp = highest frequency part of date 
                    # e.g. for 'YYYYMM', this is 'month'
                    if sp in ['year']:
                        # Assume bound covers a single year
                        td_gap = np.array([ mt.get_dpy(calendar, d['year']) 
                                            for d in ld ])
                    elif sp in ['month']:
                        # Assume bound covers a single month
                        td_gap = np.array([ dpm[ int(d['month']-1) ] 
                                            for d in ld ])
                    elif sp in ['day']:
                        # Assume bound covers a single day
                        td_gap = np.ones(ntim)
                    elif sp in ['hour']:
                        # Determine gap between subsequent times and assume
                        # the bound covers that
                        assert len(td) > 1
                        dt = np.diff(td)
                        if len(dt) > 1: 
                            assert np.allclose( dt[0], dt[1:] ), \
                                'time spacing is not constant'
                        td_gap = dt*np.ones(ntim)
                    else:
                        raise Exception('Unknown time part: {}'.format(sp))
                        
                else:
                    raise Exception('Unknown time_bnds_method: {}'.format(time_bnds_method))

                if frequency in ['6hr', '6hrPt']:
                    # Same logic as for 3hr data, which is handled in the
                    # time_input='model timestep' section below.
                    # I'm restricting use of the 'accumulated' flag here to 6hr
                    # data simply because lower frequencies (daily, monthly,
                    # yearly) already work fine.
                    if accumulated:
                        # The last timestep in the file should
                        # indicate the end of the accumulation period.
                        # Since td_first is set above to indicate the time
                        # found in file, subtract the gap from it so that
                        # it indicates the start of the accumulation period.
                        td_first -= td_gap
                    else:
                        # td_first indicates the sampled time. We want this value
                        # to be in the middle of the time bounds so that the time
                        # grid and time string correctly indicates the sampling
                        # time. So subtract half the sampling interval, so that
                        # CMOR will correctly set the time grid and filename time
                        # string.
                        td_first -= td_gap/2.
                        
                    if accumulated:
                        # 24sep.19: not clear if we can provide 6-hourly data
                        # other than sampled. So for now only allow conversion
                        # of sampled 6-hourly data.
                        raise Exception('Disabling frequency={} with cell_methods="{}" since accumulated data is not available'\
                            .format(frequency, cell_methods))

                td_last = td_first + td_gap
                td_bnds = np.array( zip(td_first, td_last) )
                
            elif time_input in ['model timestep']:
                # tv = no. of timesteps since start of model run

                if validate_time_ranges:
                    # Check that timestep is within the valid ranges based on
                    # chunk.
                    assert np.all( tv >= ts0 )
                    assert np.all( tv <= ts1 )
                
                # td = no. of days since start of model run
                # delt = model timestep, in seconds
                # spd = seconds per day
                td = tv*delt/spd
                td_ref = mt.date2time(d_ref, d_model_start, calendar, 
                    t_units=td_units)
                td -= td_ref
                
                assert np.all( np.diff(td) > 0 ), \
                    'Times should be monotonically increasing'

                # Create time bounds array
                #nt = len(td)
                assert len(td) == ntim
                td_first = np.array(td)
                if frequency in ['3hr', '3hrPt']:
                    td_gap = np.ones(ntim)/(hpd/3.)
                elif frequency in ['subhrPt']:
                    # Find spacing in timesteps, convert it to spacing in days
                    d0 = np.diff(tv)
                    dt = d0[0]
                    assert np.all(dt == d0), 'Timestep spacing in ccc file is not constant'
                    del d0
                    td_gap = float(dt*delt)/spd
                else:
                    raise Exception('How to handle frequency {}?'.format(frequency))

                if accumulated:
                    # The last timestep in the file should
                    # indicate the end of the accumulation period.
                    # Since td_first is set above to indicate the time
                    # found in file, subtract the gap from it so that
                    # it indicates the start of the accumulation period.
                    td_first -= td_gap
                else:
                    # td_first indicates the sampled time. We want this value
                    # to be in the middle of the time bounds so that the time
                    # grid and time string correctly indicates the sampling
                    # time. So subtract half the sampling interval, so that
                    # CMOR will correctly set the time grid and filename time
                    # string.
                    td_first -= td_gap/2.
                    
                td_last = td_first + td_gap
                td_bnds = np.array( zip(td_first, td_last) )
                
            else:
                raise Exception('Unknown type of time input: {}'.format(time_input))

                
            if time_output in ['seconds']:
                ts      = spd*td
                ts_bnds = spd*td_bnds
                dv['time'] = ts
                dv['time_bnds'] = ts_bnds
            elif time_output in ['days']:
                dv['time'] = td
                dv['time_bnds'] = td_bnds
            else:
                raise Exception('Unknown type of time output: {}'.format(time_output))
                
        
        else:   # if not convert_time_units
        
            tv = dv['time']  # time grid as provided in the ccc file
            
            if len(tv) == 1: # time dimension has size=1
                dv['time_bnds'] = np.concatenate((tv, tv),0)
            else:
                raise Exception('How should time dimension be handled?')
        #######################################################################
                
        varname = v.varname
        varname = varname.strip() # remove leading/trailing blank spaces
        
        dv.update({
            'units'         : cccunits,
            'model'         : model,
            'ref_date_str'  : ref_date_str,
            'dimensions'    : ccc_dims,
        })
        
        assert varname not in d_var
        d_var[varname] = dv
    ###########################################################################
    # end of loop over variables in the ccc file
    ###########################################################################
    
    # Dict d_var now holds all variables found in the file. 
    # Each key of dict d_var is a variable.
    # The info in d_var is just the contents of the ccc file, but it's been
    # rearranged into a form suitable for CMOR-izing, and info about the data
    # (units etc) is included.
    
    if one_field_per_input_file:
        assert len(d_var) == 1

    if show_d_var_info:
        show_all_field_info(d_var)
    
    return d_var, d_table

###############################################################################
# Decorator to catch exceptions within agcm_to_cmor()
def catch_exc(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as exc:
            tu.amor_exception_handler(exc, kwargs['d_return'])
    return wrapper

# Main function for converting AGCM data to CMORized netcdf       
@catch_exc
def agcm_to_cmor(
        cmortable       = None,
        source_ctabdir  = None,
        cmorvar         = None,
        cccunits        = None,
        cccts           = None,
        cccvar          = None,
        cccdiagfile     = None,
        chunk           = None,
        dimensions      = None,
        frequency       = None,
        optdeck         = None,
        user_f          = 'cccma_user_input.json',
        production      = False,

        ccchistory      = None,
        ccccomment      = None,
        strictfail_in   = False,
        cmor_logfile    = None,

        d_return        = None,
    ):
    '''Load ccc file (a data file in CCCma binary format) and save its data 
    as a CMOR-ized netcdf file.
    
    Parameters
    ----------
    cmortable : str
        Name of json file containing info on the CMOR table
        e.g. "CMIP_Amon.json"
    source_ctabdir :
        directory containing "cmortable"
    cmorvar : str
        CMOR variable name, e.g. "tas"
    cccunits : str
    cccts : str
    chunk : str
        Indicates times in the input file, e.g. '5201_5201'
    dimensions : str
    frequency : str
    optdeck : str
    user_f : str
    ccchistory : str
    ccccomment : str
    strictfail_in : bool
    cmor_logfile : str
    production : bool
    
    '''
    if d_return is None: d_return = {}
    d_return.update({
        # d_return is a dict to return info to calling program if agcm_to_cmor() has been called via multiprocessing.
        # Set defaults for it here.
        'success' : False,
        'cmor_final_filename' : '',
    })
    assert 'exc_type' not in d_return, 'exc_type is only set in d_return if an exception occurs'

    msg( '\n * starting agcm_to_cmor * ' )

    show_dimensions_info = True
    show_elapsed_time = True

    if show_elapsed_time:
        # time how long it takes to convert this variable
        time_start = time.time()
    msg( datetime.datetime.now().strftime('%d %b %Y, %H:%M:%S') \
        + ', processing ' + cmorvar + '_' + cmortable )
    
    # Remove .json extension from cmortable 
    # e.g. "CMIP_Amon.json" --> "CMIP_Amon"
    if cmortable.endswith('.json'): cmortable = os.path.splitext(cmortable)[0]

    # Load ccc-format time series file
    one_field_per_input_file = True
    d = {
        'cmortable'     : cmortable,
        'cmorvar'       : cmorvar,
        'cccunits'      : cccunits,
        'cccts'         : cccts,
        'chunk'         : chunk,
        'dimensions'    : dimensions,
        'frequency'     : frequency,
        'optdeck'       : optdeck,
        'user_f'        : user_f,
        'production'    : production,
        'one_field_per_input_file' : one_field_per_input_file,
        }
    d_var, d_table = load_ccc(**d)



    ###########################################################################
    ###########################################################################
    test_netcdf = False
    if test_netcdf:
        # Jan 2021 test, JA: try simple replacement of d_var with data loaded from basic netcdf file output from CanDIAG.
        # https://gitlab.science.gc.ca/CanESM/ncconv/issues/55
        # Parking this test code here for possible future use.
        
        import netCDF4 # or better to use xarray?
        
        # sample file from Neil, 8jan.21
        field = 'ST'
        filepath = './{}_5550.nc'.format(field)

        f = netCDF4.Dataset(filepath, 'r')
        print('loaded file: ' + filepath)

        fv = f.variables[field]
        d_var2 = {field : {
            # For reference I'm listing here all the keys in d_var for the field, even though currently
            # most can't be set from the netcdf file (8jan.21).

            #'model' : ,
            
            'field' : np.array(fv[:]),
            #'units' : ,
            'dimensions' : list(fv.dimensions), 
            #'ibuf', 

            'time' : f.variables['time'][:], 
            #'time_bnds', 
            'ref_date_str' : f.variables['time'].units, 

            #'level', 
            #'level_type', 
            #'level_units', 

            'longitude' : f.variables['longitude'][:],

            'latitude' : f.variables['latitude'][:], 
        }}
        
        dv = d_var2[field]
        set_dims = ['latitude', 'longitude', 'level', 'time']
        nc_dims = list(dv['dimensions'])
        if nc_dims != set_dims:
            # append size=1 dimensions for any that are missing
            ad = dv['field']
            shp = tuple(ad.shape)
            for dn in set_dims:
                if dn not in nc_dims:
                    shp += (1,)
                    nc_dims.append(dn)
            assert len(shp) == len(set_dims)
            assert set(nc_dims) == set(set_dims)
            ad.shape = shp
            perm = [nc_dims.index(dn) for dn in set_dims]
            dv['field'] = ad.transpose(perm)
            dv['dimensions'] = list(set_dims)
            del ad
        # Remove the redundant longitude that is stored in ccc files
        assert dv['dimensions'][1] == 'longitude'
        dv['field'] = dv['field'][:,:-1,:,:]
        assert np.mod(dv['longitude'][0], 360.) == np.mod(dv['longitude'][-1], 360.)
        dv['longitude'] = dv['longitude'][:-1]


        # Compare to d_var loaded from ccc file
        dv, dv2 = d_var[field], d_var2[field]
        for k in dv:
            if k not in dv2: continue
            print(k)
            if k == 'field':
                print(dv[k].shape, dv2[k].shape)
            else:
                print(dv[k])
                print(dv2[k])
            print()
  
        d_var[field].update(d_var2[field])
        del d_var2
    ###########################################################################
    ###########################################################################
    
    
    ###########################################################################
    # Convert variable to CMOR-ized netcdf using the CMOR python API.
    msg( '\n * starting CMORization * ' )
    
    cccvar = cccvar.upper() 
    # table_utils.py converts cccvar to lowercase, but for cccma_py output we
    # need uppercase

    # Some variables have a name in the ccc file that differs from the one used
    # in the time series file name (cccts, which is the name in the google
    # spreadsheet's "CCCma TS var name" column). 
    if one_field_per_input_file:
        # If the ccc file only contains one variable, assume that it's the
        # correct one.
        assert len(d_var) == 1
        if cccvar not in d_var:
            cccvar = d_var.keys()[0]
    
    if cccvar not in d_var:
        msg('CCCma time series variable not found in ccc file: ' + cccvar \
            + ' (variables in file: ' + ', '.join( sorted(d_var.keys()) ) \
            + ')', excep=True)
    
    dv = d_var[cccvar]
    # dv = dict containing the desired variable from the ccc file
    model           = dv['model']
    ref_date_str    = dv['ref_date_str']
    
    # CMOR tables location and setup
    # 'netcdf_file_action' sets the output netcdf file type. Valid options:
    #   cmor.CMOR_REPLACE_4     netCDF-4 classic model
    d = {
        'inpath'                : PATH_CMORTABLES
    ,   'netcdf_file_action'    : cmor.CMOR_REPLACE_4
    }
    if cmor_logfile and isinstance(cmor_logfile, (str, unicode)): 
        d.update({'logfile' : cmor_logfile})
    cmor.setup(**d)
    
    # Define user json file
    cmor.dataset_json(user_f)
    
    # Load CMOR table (aka "MIP table") for the variable.
    # E.g. for Amon in CMIP6 data request, cmortable = 'CMIP6_Amon'.
    cmor.load_table(cmortable + '.json')
            
    ad = dv['field']  
    # ad : data array. This is the array containing the physical field that
    # is being written to the netcdf file.
    
    # Set missing value. This is the number that signifies missing values in
    # the input file. CMOR will automatically change it to the value it prefers
    # to use for missing values, which seems to be 1.e20. I'm not sure if this
    # preferred value is set in one of the input/config json files or is just
    # hard-coded into CMOR. But that doesn't really matter, since we don't
    # want to change this value. 
    #
    # In other words, if the ccc files have used 1.e38 to indicate missing
    # values, the set misval=1.e38. CMOR will then change those values in the
    # output nc file to its preferred value (1.e20).
    misval = 1.e38
    
 
    ###########################################################################
    # Define the dimensions in the ccc field, and the requested dimensions.
    #
    # 'dims' identifies the order of dimensions of the data array 'ad'.
    # This is the order of dims produced by loading the data from the ccc file
    # using cccma_py. It is NOT the order of dims required by CMOR.
    #
    # Do not redefine 'dims' after this point unless the corresponding
    # dimensions of 'ad' are also redefined. 
    dims = ['latitude', 'longitude', 'level', 'time']

    # Check that dimensions are as expected.
    assert dims == chksme(dims), 'dims should have no repeated dimensions'
    assert len(dims) == len(ad.shape), 'dims does not match data array shape'

    # d_axis_dims maps the dimension name given in 'dims' (i.e. for the ccc
    # input file) to the 'axis' attribute from the CMOR tables.
    d_axis_dims = {
        # dimension       # axis
        'latitude'      : 'Y'
    ,   'longitude'     : 'X'
    ,   'level'         : 'Z'
    ,   'time'          : 'T'
    }

    ###########################################################################
    # Rearrange dimensions of data array 'ad' if required.
    # ccc-format files are limited to four dimensions (3 space & time).
    # Some output fields have more dimensions than this and the field has
    # been shoehorned into ccc format by concatenating dimensions into a
    # single dimension (as is done when flattening a numpy array). Here
    # is where the dimensions can be rearranged into the desired form.
    # The result is as if the ccc file had been able to originally contain
    # the desired number of dimensions. Hence this conversion acts only on
    # 'ad' and 'dims', and is done prior to the stuff below that maps the ccc
    # file contents to the requested dimensions.
    expand_dim = None # None ==> don't alter the dimensions

    # Rename dimensions if required.
    # This is like a much simpler version of the expand_dim option. Use it
    # if the ccc file has used one of its conventional dimensions, e.g.
    # 'level', to store some other dimension.
    # In this case we also assume that grid values for the renamed dimension
    # are available from the data request, and that the grid for the
    # overwritten dimension should be removed from dict 'dv'.
    replace_dim = None

    # Specify grid values based on the data request (i.e. CMIP6_*.json files)
    # rather than what's in the ccc file. For each grid listed in set_grid,
    # entries of dict dv are created or updated to contain the requested grid
    # values. This means that we are assuming the ccc file is on the correct
    # grids, and it's up to the model and/or optional deck to take care of
    # that. Hence this option should be used with caution.
    set_grid = None # None ==> don't set any grids

    d_coord = d_table['CMIP6_coordinate']['axis_entry']
    if   (  dimensions  == 'longitude latitude plev7c tau time' \
            and optdeck == 'cospmask_prestau_v2' ):
        expand_dim = {
            'level'     : ['tau', 'level'] # order here corresponds to the order given in the ccc file (use ggstat to find out)
        }
        new_dim_size = {'tau' : 7, 'level' : 7}
        dn = 'tau'
        d_axis_dims.update({dn : d_coord[dn]['axis']})
        set_grid = {'tau' : 'tau', 'level' : 'plev7c'}

    elif (  dimensions  == 'longitude latitude effectRadIc tau time' \
        and optdeck     == 'cospmask_refftau' ):
        expand_dim = {
            'level'     : ['effectRadIc', 'tau'] # order here corresponds to the order given in the ccc file (use ggstat to find out)
        }
        new_dim_size = {'effectRadIc' : 6, 'tau' : 7}
        d_coord['tau']['axis'] = 'tau' # prevent more than one axis named '' (axis names cannot be repeated)
        for dn in ['effectRadIc', 'tau']:
            d_axis_dims.update({dn : d_coord[dn]['axis']})
        set_grid = {'effectRadIc' : 'effectRadIc', 'tau' : 'tau', 'level' : None}

    elif (  dimensions  == 'longitude latitude effectRadLi tau time' \
        and optdeck     == 'cospmask_refftau' ):
        expand_dim = {
            'level'     : ['effectRadLi', 'tau'] # order here corresponds to the order given in the ccc file (use ggstat to find out)
        }
        new_dim_size = {'effectRadLi' : 6, 'tau' : 7}
        d_coord['tau']['axis'] = 'tau' # prevent more than one axis named '' (axis names cannot be repeated)
        for dn in ['effectRadLi', 'tau']:
            d_axis_dims.update({dn : d_coord[dn]['axis']})
        set_grid = {'effectRadLi' : 'effectRadLi', 'tau' : 'tau', 'level' : None}
        
    elif (  dimensions  == 'longitude latitude sza5 time' \
        and optdeck     == 'cospmask_parasolrefl' ):
        dn = 'sza5'
        replace_dim = {'level' : dn}
        d_axis_dims.update({dn : d_coord[dn]['axis']})

    elif (  dimensions  == 'longitude latitude alt16 tau time' \
        and optdeck     == 'cospmask_misr_alt_tau' ):
        expand_dim = {
            'level'     : ['level', 'tau'] # order here corresponds to the order given in the ccc file (use ggstat to find out)
        }
        new_dim_size = {'level' : 16, 'tau' : 7}
        dn = 'tau'
        d_axis_dims.update({dn : d_coord[dn]['axis']})
        set_grid = {'tau' : 'tau', 'level' : 'alt16'}

    elif (  dimensions  == 'longitude latitude alt40 dbze time' \
        and optdeck     == 'PLACEHOLDER' ):  # will be for cfadDbze94_Emon
        stop
        # 3sep.19: this is guess at what will work, but need to check the order of grids
        dn = 'dbze'
        expand_dim = {
            'level'     : ['level', dn] # order here corresponds to the order given in the ccc file (use ggstat to find out)
        }
        new_dim_size = {'level' : 40, dn : 15} # there are 15 requested values for dbze
        d_axis_dims.update({dn : d_coord[dn]['axis']})
        set_grid = {dn : dn, 'level' : 'alt40'}
        
    elif (  dimensions  == 'longitude latitude alt40 scatratio time' \
        and optdeck     == 'PLACEHOLDER' ): # will be cfadLidarsr532_Emon
        stop
        # 3sep.19: this is guess at what will work, but need to check the order of grids
        dn = 'scatratio'
        expand_dim = {
            'level'     : ['level', dn] # order here corresponds to the order given in the ccc file (use ggstat to find out)
        }
        new_dim_size = {'level' : 40, dn : 15} # there are 15 requested values for scatratio
        d_axis_dims.update({dn : d_coord[dn]['axis']})
        set_grid = {dn : dn, 'level' : 'alt40'}

    elif (  dimensions == 'site time1' \
        and optdeck    == 'maskcdcb' ):
        dn = 'site'
        replace_dim = {'latitude' : dn}
        d_axis_dims.update({dn : d_coord[dn]['axis']})

    if expand_dim is not None:
        for dn in expand_dim:
            new_dims = expand_dim[dn]
            dim = dims.index(dn)
            shp_new_dims = [new_dim_size[ndn] for ndn in new_dims]
            assert ad.shape[dim] == np.prod(shp_new_dims)

            show_shapes = not True
            if show_shapes:
                print(dims)
                print(ad.shape)

            # Move the dimension being expanded to be the last dimension of the data array (ad)
            n = len(ad.shape)
            perm = range(n)
            perm[dim] = n-1
            perm[n-1] = dim
            ad = np.transpose(ad, perm)
            # Keep 'dims' order consistent with 'ad' shape
            dims[dim] = dims[n-1]
            dims[n-1] = dn

            if show_shapes:
                print(dims)
                print(ad.shape)

            # Create shape tuple for the new desired data array shape
            shp = list(ad.shape)[:n-1]
            shp += shp_new_dims
            
            # Give the data array the new shape
            ad.shape = tuple(shp)
            # Make 'dims' consistent with the new shape
            dims = dims[:n-1] + expand_dim[dn]

            if show_shapes:
                print(dims)
                print(ad.shape)

            # add comments explain how it works

    ###########################################################################    
    def set_req_grid(dv, gn, gn_req):
        '''In dict 'dv', set grid 'gn' to have the array,units,etc of 
        requested grid 'gn_req'.
        
        This is used to "impose" a requested grid on the ccc file data, as if
        the ccc file had contained that grid information. It should only be
        used if we're sure that the ccc file does indeed use that grid. This
        would be determined by the time series file (the model diagnostic
        output) as well as any optional deck that's applied to the time series
        file.
        '''
        d_coord = d_table['CMIP6_coordinate']['axis_entry']
        # (This function is defined here, within agcm_to_cmor(), because
        # it requires d_table in its scope. Also it's only used at this point,
        # so it's convenient to have the code for it right here.)
        assert gn_req in d_coord, 'Unknown coordinate grid: ' + gn_req
        assert gn not in dv, 'Grid {0} already exists in the ccc file - should it be replaced?'.format(gn)
        dg = d_coord[gn_req]
        
        if 'requested' in dg:
            gr =      np.array([float(s) for s in dg['requested']])
            gr_bnds = np.array([float(s) for s in dg['requested_bounds']])
        else:
            # Sometimes requested values aren't in dg['requested']. 
            # Add other cases here if necessary.
            # See get_requested_levels() for guidance if needed (it
            # handles some cases like this for vertical levels). 
            raise Exception('Specify how to get the requested grid values')
        dv.update({
            gn              : gr,
            gn + '_units'   : dg['units'],
            gn + '_type'    : dg['long_name'],
        })
        if dg['must_have_bounds'].lower() == 'yes':
            gr_bnds.shape = (len(gr), 2)
            dv.update({
                gn + '_bnds'    : gr_bnds,
            })
    def remove_dv_grid(dv, gn):
        '''Remove grid info for grid 'gn' from dict 'dv'.'''
        if gn in dv:
            l = ['', '_bnds', '_units', '_type']
            for s in l:
                p = gn + s
                if p in dv: 
                    dv.pop(p)
    ###########################################################################
    
    if replace_dim is not None:
        for dn in replace_dim:
            dn_req = replace_dim[dn]
            dim = dims.index(dn)
            dims[dim] = dn_req

            remove_dv_grid(dv, dn)
            set_req_grid(dv, dn_req, dn_req)

    if set_grid is not None:
        d_levs_req = get_requested_levels( d_table['CMIP6_coordinate'] )
        for gn in set_grid:
            # gn = name the grid has (or will have) in dict dv
            # gn_req = name of the requested grid in the CMIP6 CV (aka data
            # request, aka CMIP6_*.json files)

            gn_req = set_grid[gn]
            if gn_req is None:
                # Setting gn_req=None indicates that info for this grid
                # should be removed.
                remove_dv_grid(dv, gn)
            elif gn_req in d_levs_req:
                dg = d_levs_req[gn_req]
                dv.update({
                    'level'         : dg['levels'],
                    'level_units'   : dg['units'],
                    'level_type'    : dg['type'],
                })
                if 'bnds' in dg:
                    dv['level_bnds'] = dg['bnds']
            else:
                set_req_grid(dv, gn, gn_req)

    # Some code below assumes that the levels dimension is array index=2.
    # Ensure that this remains the case. (The reason to check is that it's
    # possible that the expand_dim or replace_dim options, above, could
    # have altered 'dims' and 'ad'.)
    dn = 'level'
    if dn in dims:
        dim = dims.index(dn)
        dim_lev = 2
        if dim != dim_lev:
            n = len(ad.shape)
            perm = range(n)
            perm[dim] = dim_lev
            perm[dim_lev] = dim
            ad = np.transpose(ad, perm)
            # Keep 'dims' order consistent with 'ad' shape
            dims[dim] = dims[dim_lev]
            dims[dim_lev] = dn

    # Check again, after doing any array shape manipulations, that data
    # array dimensions remain consistent with 'dims'.
    assert dims == chksme(dims), 'dims should have no repeated dimensions'
    assert len(dims) == len(ad.shape), 'dims does not match data array shape'

    ###########################################################################
    
    s = d_table[cmortable]['variable_entry'][cmorvar]['dimensions']
    if dimensions != s:
        msg('Dimensions mismatch. Input dimensions: ' + dimensions \
            + ', requested dimensions: ' + s, excep=VALIDATE_CMORVAR_INFO)
    
    # dims_req defines the requested dimensions for the variable.
    # These are the dimensions specified in the CMIP6 data request.
    dims_req = dimensions.split()
    assert dims_req == chksme(dims_req), \
        'dims_req should have no repeated dimensions'

    # A mapping is needed from ccc file dimensions to requested dimensions.
    # To find this mapping, compare "axes", rather than dimension names.
    # Here an "axis" refers to a spatial direction or time. 
    # The dimension names for ccc files were defined above, but since the data
    # request has many different kinds of grid/dimension it's not possible to
    # choose names for the ccc dimensions that cover all cases in the data
    # request. 
    d_coord = d_table['CMIP6_coordinate']['axis_entry']
    axis_dims = [d_axis_dims[dn] for dn in dims]
    axis_dims_req = []
    for dn in dims_req:
        if dn in MODEL_LEVELS_REQ: ax = 'Z'
        else: ax = str(d_coord[dn]['axis'])
        axis_dims_req += [ax]
 
    if False:
        print(dims)
        print(dims_req)
        print(axis_dims)
        print(axis_dims_req)
        
    assert axis_dims == chksme(axis_dims), \
        'axis_dims should have no repeated dimensions'
    assert axis_dims_req == chksme(axis_dims_req), \
        'axis_dims_req should have no repeated dimensions'
    if show_dimensions_info:
        # Display info about the given and requested dimensions.
        lw = []
        lw += ['ccc file:']
        lw += ['    dimensions: ' + ', '.join(dims)]
        lw += ['          axes: ' + ', '.join(axis_dims)]
        lw += ['requested:']
        lw += ['    dimensions: ' + ', '.join(dims_req)]
        lw += ['          axes: ' + ', '.join(axis_dims_req)]
        msg('\n'.join(lw))
    # For each requested axis, check that it's present in the ccc input file
    for ax in axis_dims_req:
        if ax in ['']:
            # For some requested dimensions no axis is given. 
            # E.g. 'typebare' has axis='' in CMIP6_coordinate.json.
            # ccc files are defined here to contain lat, lon, level, and time
            # axes. There's nothing that corresponds to "no axis". So for the
            # "no axis" case the comparison with axis_dims is skipped.
            # The next check, below, requires that any axis in the ccc file
            # that isn't requested must have size=1. This should catch any
            # unaccounted-for axes in the ccc file.
            continue
        assert ax in axis_dims, 'Expected to find axis: ' + ax
    # For each axis in the input ccc file, check that its size is 1 if it's
    # not a requested axis
    for ax in axis_dims:
        if ax not in axis_dims_req:
            # If a dimension in dims isn't requested, it should have size 1.
            dim = axis_dims.index(ax) 
            # dim = index of this dimension in the data array
            assert ad.shape[dim] == 1, \
                'For axis ' + ax + ', unexpected size: ' + str(ad.shape[dim])

    # Create mapping from input file dimension name (in 'dims') to output file
    # dimension name (in 'dims_req')
    d_dn = {}
    for k,ax in enumerate(axis_dims): 
        dn = dims[k]
        if ax in axis_dims_req:
            m = axis_dims_req.index(ax)
            dn_req = dims_req[m]
            d_dn[dn] = str(dn_req)
    ###########################################################################

    axis_ids = []
    d_cmor_axis = {} # dict to store defined CMOR axes
    d_cmor_zfactor = {} # dict to store defined CMOR zfactors
    write_ps = not True # flag used for model levels data
    ps = None
    dims_ps = None
    time_ps = None

    # Loop over the dimensions of the ccc file data.
    # Define CMOR dimensions where necessary.
    for dim,dn in enumerate(dims):

        g = dv[dn]
        # g = grid for this dimension from the ccc file
        b = None
                    
        # If dimension has size=1 there's no need to define a CMOR dimension
        # for it. Size=1 dimensions are ok in the data array. CMOR will remove
        # them when it writes the output netcdf file. It's not necessary to
        # remove them here and is simpler not to.
        #if len(g) <= 1:
        if g is None:
            # This dimension should be ignored.
            assert ad.shape[dim] == 1, 'Ignored array dimension must have size=1'
            continue
        else:
            assert isinstance(g, np.ndarray), \
                'Grid expected to be array, instead found type = {}'.format(type(g))
        if len(g) <= 1 and dn not in ['site']:
            # This dimension should be ignored, but confirm that the grid provided
            # matches the dimension size.
            assert ad.shape[dim] == len(g), 'ad.shape={}, len(g)={}, dim={}, dn={}'\
                .format(ad.shape, len(g), dim, dn)
            continue

        if        dn in ['latitude']:
    
            #b = get_bounds(g, -90, 90) 
            # The above get_bounds() call gives the bounds as the arithmetic
            # mean of the grid values. 
            # The Gaussian grid latitude bounds from the Gaussian weights are
            # provided by the get_lats() function. 
            lat, lat_bnds = get_lats(model)

            # Now check that the latitudes are as expected. 
            # get_lats() returns the Gaussian latitudes (lat) and their
            # corresponding latitude bounds (lat_bnds) based on the Gaussian
            # weights. We already know the latitudes, these are stored in
            # array g. They should match the lat grid from get_lats().
            assert np.allclose(g, lat, rtol=1e-8, atol=1e-10), \
                'Unexpected Gaussian grid latitudes'
            b = lat_bnds
            
            d = {
                'table_entry'       : 'latitude'
            ,   'units'             : 'degrees_north'
            ,   'coord_vals'        : g
            ,   'cell_bounds'       : b
            }
            d_cmor_axis[dn] = cmor.axis(**d)
            axis_ids += [ d_cmor_axis[dn] ]
            
        elif    dn in ['longitude']:
            
            # The longitude grid is evenly spaced. The arithmetic mean,
            # provided by get_bounds(), is appropriate for the longitude
            # bounds.
            b = get_bounds(g)
            d = {
                'table_entry'       : 'longitude'
            ,   'units'             : 'degrees_east'
            ,   'coord_vals'        : g
            ,   'cell_bounds'       : b
            }
            d_cmor_axis[dn] = cmor.axis(**d)
            axis_ids += [ d_cmor_axis[dn] ]
            
        elif    dn in ['level']:
            # Terminology (variable names) for vertical dimension/grid names:
            # dn : dimension name from ccc file, which is 'level'
            # dn_req : dimension name requested by the dreq
            #       e.g. 'alevel', 'plev19', etc
            # level_set : name of requested altitude grid as given in the
            #       CMIP6_coordinates.json table
            # For pressure level sets, dn_req & level_set are the same.
            # For model levels, dn_req is 'alevel' or 'alevhalf' and level_set
            # indicates the specific type of vertical level used in our model.

            # First get some required info.
            #   z_grids: lists of all possible requested altitude grids
            #   d_levs_req: contains requested levels (if they exist)
            #   d_levs_req2out: maps from requested levels to levels used in the output
            z_grids = get_requested_z_grids( d_table['CMIP6_coordinate'] )
            d_levs_req = get_requested_levels( d_table['CMIP6_coordinate'] )
            d_levs_req2out = D_MODEL[model]['resolution']['d_levs_req2out']

            dn_req = d_dn[dn]
            assert dn_req in z_grids
            level_set = dn_req
            if level_set in d_levs_req2out: 
                level_set = d_levs_req2out[level_set]

            # Get list of level sets for which specific levels are requested
            # (e.g. pressure level sets)
            requested_level_sets = [gn for gn in d_levs_req 
                                    if len(d_levs_req[gn]['levels']) > 0]

            if dn_req in MODEL_LEVELS_REQ:
                # Requested levels are AGCM model levels or model half levels.
                #
                # Since the data are requested on model levels, assume that
                # the full set of levels in the input file can be used in the
                # output file.
                #
                # ccc files store model level values as 1000*eta, where eta is
                # the vertical coordinate value, a number between 0 and 1.
                # The surface is 1.
                #
                # For more info on how the AGCM levels work, see docstrings
                # in the coordab() and get_lev_bnds() functions.

                lev = g/1e3 # get eta values from ccc file levels
                del g
                d = D_MODEL[model]['resolution']
                lev_bnds = get_lev_bnds(lev, d['lay'], d['plid'], d['pref'])
                    
                model_level_units = D_MODEL[model]['resolution'][dn_req + ' units']
                assert dv['level_units'] == model_level_units, \
                    'Error in model levels units. Got: {}, expected: {}'.format(dv['level_units'], model_level_units)
                
                d = {
                    'table_entry'   : level_set,
                    'units'         : dv['level_units'],
                    'coord_vals'    : lev,
                    'cell_bounds'   : lev_bnds,
                }
                d_cmor_axis[dn] = cmor.axis(**d)
                axis_ids += [ d_cmor_axis[dn] ]
                
                if level_set in ['alternate_hybrid_sigma', \
                                 'alternate_hybrid_sigma_half']:
                    # Get surface pressure
                    dim = dims.index(dn)
                    shp = list(ad.shape)
                    shp[dim] = 1
                    shp = tuple(shp)
                    units_ps = 'Pa'
                    dims_ps = [dn1 for dn1 in dims if dn1 != dn] # surface pressure (ps) has all dims except vertical
                    write_ps = True
                    load_ccc_ps = True
                    ps = None
                    if load_ccc_ps:
                        # Load surface pressure data from ccc file 
                        cccvar_ps = 'PS' 
                        # cccvar_ps is the variable name for surface pressure in the
                        # 'CCCma TS var name' column of the output_variables spreadsheet.
                        tab_name = re.sub("CMIP6_","",cmortable)
                        tab_name = re.sub("\.json","",tab_name)
                        suffix   = "{}_for_AGCM_ML_{}".format(cccvar_ps.lower(),tab_name)
                        ccc_pfx  = cccts.rpartition( '_' + cccdiagfile )[0] # e.g. cccdiagfile = 'gs', 'ds', etc
                        cccts_ps = "{}_{}".format(ccc_pfx,suffix)
                        if not os.path.exists(cccts_ps):
                            print("generating surface pressure file for model level conversion for {} in {}".format(cmorvar,tab_name))
                            generate_surface_pressure_for_model_levs(ccc_pfx, suffix, tab_name, source_ctabdir, cccvar_ps.lower())
                        else:
                            print("using already generated {} for model level conversion for {} in {}".format(cccts_ps, cmorvar, tab_name))
                        cmorvar_ps = 'ps'
                        dimensions_ps = d_table[cmortable]['variable_entry'][cmorvar_ps]['dimensions']

                        l = [s for s in dimensions_ps.split() if 'time' in s] # find the name of the time dimension (e.g. 'time', 'time1')
                        assert len(l) == 1, 'surface pressure should have only one time dimension'
                        time_ps = l[0]

                        if False:
                            print(dims)
                            print(dims_ps)
                            print(dimensions)
                            print(dimensions_ps)
                        
                        d = {
                            'cmortable'     : cmortable,
                            'cmorvar'       : cmorvar_ps,
                            'cccunits'      : units_ps,
                            'cccts'         : cccts_ps,
                            'chunk'         : chunk,
                            'dimensions'    : dimensions_ps,
                            'frequency'     : frequency,
                            'user_f'        : user_f,
                            'production'    : production,
                            'one_field_per_input_file' : one_field_per_input_file,
                            }
                        d_var_ps, d_table_ps = load_ccc(**d)
                        dv_ps = d_var_ps[cccvar_ps]
                        del d_var_ps, d_table_ps
                        # Check that loaded surface pressure is consistent with
                        # grids etc of the field being converted.
                        assert dv_ps['model'] == model
                        assert dv_ps['ref_date_str'] == ref_date_str
                        for gn in ['latitude', 'longitude', 'time', 'time_bnds']:
                            assert np.allclose(dv[gn], dv_ps[gn]), '{} mismatch for surface pressure'.format(gn)
                        ps = dv_ps['field']
                        assert ps.shape == shp, 'Wrong surface pressure array shape: got {}, expected {}'.format(ps.shape, shp)
                    else:
                        #ps = 1e5*np.ones(shp)
                        #print('\n\n * * * WARNING: USING DUMMY VALUES FOR SURFACE PRESSURE * * * \n\n')
                        raise Exception('Where to get surface pressure for model levels output file?')
                    if ps is None:
                        raise Exception('Unable to find surface pressure data')
                    check_ps_range = True
                    if check_ps_range:
                        # Check that surface pressure values lie in the expected range.
                        # Use a wide range. This is just checking for the correct order of magnitude.
                        if units_ps == 'Pa':
                            ps_min, ps_max = 2e4, 2e5
                        else:
                            raise Exception('Unknown units for surface pressure: ' + units_ps)
                        ok = np.all(ps > ps_min) and np.all(ps < ps_max)
                        if not ok:
                            raise Exception('Surface pressure is outside allowed range of {} to {} {}'.format(ps_min, ps_max, units_ps))
                    # Surface pressure also needs to be written to the netcdf file,
                    # but this is done further below (the write_ps flag controls this).

                    # Get A,B level coefficients for the model levels ('lev') as
                    # well as their bounds ('lev_bnds').
                    d = D_MODEL[model]['resolution']
                    ap, b           = coordab(lev,      d['coord'], d['plid'], d['pref'])
                    ap_bnds, b_bnds = coordab(lev_bnds, d['coord'], d['plid'], d['pref'])
                    # Create CMOR zfactors corresponding to the A,B coefficients.
                    d = {
                        'zaxis_id'          : d_cmor_axis[dn],
                        'axis_ids'          : [d_cmor_axis[dn],],
                        'zfactor_name'      : 'ap',
                        'units'             : units_ps,
                        'zfactor_values'    : ap,
                        'zfactor_bounds'    : ap_bnds,
                    }
                    cmor.zfactor(**d)
                    d = {
                        'zaxis_id'          : d_cmor_axis[dn],
                        'axis_ids'          : [d_cmor_axis[dn],],
                        'zfactor_name'      : 'b',
                        'zfactor_values'    : b,
                        'zfactor_bounds'    : b_bnds,
                    }
                    cmor.zfactor(**d)

                else:
                    raise Exception('Unknown type of model level: ' + level_set)

            
            elif dn_req in requested_level_sets:
                # Requested levels are a specific set, i.e. the values of the
                # levels are given in the data request. E.g. 'plev19', a set
                # of 19 pressure levels.
                #
                # Check here that a requested set of levels is present, and
                # extract the required levels.
                
                assert level_set in d_levs_req
                assert len(d_levs_req[level_set]['levels']) > 0

                msg('Extracting level set: ' + level_set)
                
                # gr : requested levels
                # gr_units : units of the requested levels
                # gr_type : what kind of level is it (pressure, etc)
                gr       = d_levs_req[level_set]['levels']
                gr_units = d_levs_req[level_set]['units']
                gr_type  = d_levs_req[level_set]['type']
                
                if True:
                    # confirm new way is consistent with old way (26jan.19)
                    d = d_table['CMIP6_coordinate']['axis_entry'][level_set]
                    gr2 = np.array( [float(s) for s in d['requested'] ])
                    gr_units2 = d['units']
                    assert np.all(gr2 == gr)
                    assert gr_units2 == gr_units
                
                g_conv = 1.
                # If gr_units is a pressure unit (e.g. 'Pa') then requested
                # levels are pressure levels
                if gr_units in ['Pa'] and dv['level_units'] in ['hPa']:
                    g_conv = 1e2
                    assert gr_type in ['pressure']
                elif gr_units in ['Pa'] and dv['level_units'] in ['Pa']:
                    pass
                elif gr_units in ['m'] and dv['level_units'] in ['m']:
                    pass
                else:
                    raise Exception('Unknown units for vertical levels.'\
                        + ' Requested: {}, units in ccc file: {}'\
                        .format(gr_units, dv['level_units']))
                gr /= g_conv  
                # gr now contains the requested levels, expressed in the units
                # of the ccc file's levels.
                
                if gr_type in ['pressure']:

                    #missing_levels_method = 'remove missing levels'
                    missing_levels_method = 'fill with missing values'
                    
                    p_min = D_MODEL[model]['resolution']['p_min']/1e2 # convert Pa to hPa
                    # p_min = highest altitude (lowest pressure) that the model
                    # is providing for pressure levels.
                    plid = D_MODEL[model]['resolution']['plid']/1e2 # convert Pa to hPa
                    assert p_min > plid, 'p_min = {} hPa, but it should be a lower altitude than plid = {} hPa'.format(p_min, plid)
                    if missing_levels_method in ['remove missing levels']:
                        # Remove requested levels that lie outside of what the
                        # model can provide.
                        # --> oops, this doesn't work - CMOR objects if any
                        # requested level isn't provided. Don't use this method.
                        raise Exception('invalid method for handling missing pressure levels')
                        ind = np.where( gr >= p_min )[0]
                        gr = np.take(gr, ind)
                    elif missing_levels_method in ['fill with missing values']:
                        ind_missing = np.where( gr < p_min )[0]
                        # Pretend these levels are in the ccc file by attaching
                        # them to the data array.
                        gr_missing = gr[ind_missing]
                        for gval in gr_missing: 
                            # make absolutely sure the levels are missing!
                            assert not np.any(np.isclose(gval, g, atol=1e-4))
                        g = np.concatenate((gr_missing, g), 0)
                        dim = dims.index(dn)
                        shp = list(ad.shape)
                        shp[dim] = len(gr_missing)
                        ad_missing = np.ones(tuple(shp))*misval
                        ad = np.concatenate((ad_missing, ad), dim)
                
                ind = []
                nr = len(gr)
                for m in range(nr):
                    d1 = np.where( g == gr[m] )[0]
                    if         len(d1) == 1:    # level is found
                        ind += [ d1[0] ]
                    elif    len(d1) == 0:    # level is not found
                        continue
                    else:    # should not happen
                        raise Exception('Error finding level indices:' + str(d1))
                assert len(ind) == nr, 'Not all requested levels were found'
                # Create a new array of correct shape to hold the requested
                # level set
                shp = list(ad.shape)
                assert dims.index('level') == 2 # code below assumes this is the levels dimension
                shp[2] = nr
                b = np.ones(tuple(shp))
                b.fill(np.nan)
                for k,m in enumerate(ind):
                    #b[:,:,k,:] = ad[:,:,m,:]
                    b[:,:,k] = ad[:,:,m]
                    # 16aug.19: specifying the last array dimension shouldn't be necessary.
                    # Leaving it unspecified allows this code to work if the array has more
                    # than 4 dimensions. If for some reason it becomes necessary to revert
                    # to the original version (which is commented out, above) then also do:
                    #   assert len(shp) == 4

                # Ensure all nan values were overwritten with data
                assert not any(np.isnan( b.flatten() ))
                ad = b
                # Change units of requested levels back to their original ones
                gr *= g_conv
                # Replace levels grid from ccc file with the requested levels
                g = gr
                del b, gr
                
                # Do these levels require bounds?
                if 'bnds' in d_levs_req[level_set]:
                    b = d_levs_req[level_set]['bnds']
                    if 'level_bnds' in dv:
                        # If level bounds are already present in dv, check that
                        # these are consistent with the requested bounds.
                        assert np.all(b == dv['level_bnds']), 'Inconsistent levels bounds'
                    dv['level_bnds'] = b

                d = {
                    'table_entry'   : level_set
                ,   'units'         : gr_units
                ,   'coord_vals'    : g
                }
                if 'level_bnds' in dv:
                    b = dv['level_bnds']
                    d.update({
                        'cell_bounds'   : b
                    })

                d_cmor_axis[dn] = cmor.axis(**d)
                axis_ids += [ d_cmor_axis[dn] ]

            else:
                # Levels are not a specific set (like pressure levels) and
                # are not AGCM model levels. 

                assert level_set in d_levs_req
                assert len(d_levs_req[level_set]['levels']) == 0
                assert level_set == dn_req
                    
                if dn_req in ['sdepth', 'snowdepth']:
                    # 13feb.19: 'snowdepth' not tested yet since there are
                    # no requested variables in the current google spreadsheet
                    # that use this dimension.
                    
                    gm = D_MODEL[model]['resolution'][dn_req]
                    gm_bnds = D_MODEL[model]['resolution'][dn_req + '_bnds']
                    gm_units = D_MODEL[model]['resolution'][dn_req + ' units']
                    
                    assert len(gm) == len(g)
                    assert gm_units == dv['level_units'] 
                    
                    d = {
                        'table_entry'   : level_set
                    ,   'units'         : gm_units
                    ,   'coord_vals'    : gm
                    ,   'cell_bounds'   : gm_bnds
                    }
                    d_cmor_axis[dn] = cmor.axis(**d)
                    axis_ids += [ d_cmor_axis[dn] ]
                
                else:
                    raise Exception('Unknown levels: ' + level_set)
        
        
        elif    dn in ['time']:
    
            g = np.array(g, float)
            b = np.array( dv['time_bnds'], float )
                
            table_entry = 'time'
            if 'time1' in dimensions:
                # quick fix, 30jan.19  
                table_entry = 'time1'
            
            if frequency in ['subhrPt']:
                # With output every 2 model timesteps, CMOR has problems with
                # truncating the float values given for the time grids.
                # It checks for overlap or gaps between the time bounds values
                # and finds them even there shouldn't be any.
                # The solution is to truncate the time grid and time bounds.
                # Arbitrarily I chose to keep 6 decimal places (m=1e6, below).
                # It seems to fix the problem, and I can't see why we'd need
                # more decimal places than this.
                # (Probably this could be done for any frequency of data, it
                # doesn't have to be just sub-hourly. But this problem never
                # arose for any other frequency, so for simplicity it's better
                # to just do this for sub-hourly.)
                m = 1e6
                g = np.round(m*g)/m
                b = np.round(m*b)/m
                
            d = {
                'table_entry'       : table_entry
            ,   'units'             : ref_date_str
            ,   'coord_vals'        : g
            ,   'cell_bounds'       : b
            }
            
            d_cmor_axis[dn] = cmor.axis(**d)
            axis_ids += [ d_cmor_axis[dn] ]
         
        else:
            # Assume the dimension has been provided in dv. 
            assert dn in dv, 'Dimension {} is not available in ccc file and was not created in amor.py'.format(dn)
            d = {
                'table_entry'       : dn,
                'units'             : dv[dn + '_units'],
                'coord_vals'        : dv[dn],
            }
            k = dn + '_bnds'
            if k in dv:
                # Bounds aren't needed for all kinds of dimensions. If they're
                # required but missing, CMOR will let us know.
                d.update({
                    'cell_bounds'       : dv[dn + '_bnds']
                })
                
            if dn == 'site':
                # Not sure what this grid should be. The CMIP6_coordinate.json
                # file under 'site' says
                #   "long_name": "site index"
                # and there are no units. However it does require integers.
                # If floats are passed, they get truncated.
                d['coord_vals'] = np.arange(ad.shape[dim])

            d_cmor_axis[dn] = cmor.axis(**d)
            axis_ids += [ d_cmor_axis[dn] ]

    # needed in case a null comment was cast to a string:
    if ccccomment == "None": ccccomment = None
    if ccchistory == "None": ccchistory = None
    d = {
        'original_name'     : cccvar
    ,   'history'           : ccchistory
    ,   'comment'           : ccccomment 
    ,   'missing_value'     : misval
    }
    if d_table[cmortable]['variable_entry'][cmorvar]['positive'] not in ['']:
        # In the data request, the 'positive' attribute of CMORvar items
        # is one of:
        #   ['', 'up', 'down']
        # http://clipc-services.ceda.ac.uk/dreq/u/ATTRIBUTE::CMORvar.positive.html
        #
        # By setting the 'positive' parameter in the call to cmor.variable(),
        # we tell CMOR what direction corresponds to positive in the data
        # array. CMOR then looks at the requested direction, which is given by
        #   d_table[cmortable]['variable_entry'][cmorvar]['positive']
        # (i.e. it's given by the data request) and flips the sign if
        # necessary.
        # 
        # Our practice is that the field passed to CMOR should already have
        # the correct sign (i.e. if the field in the ccc time series files did
        # not have the correct sign, then an optional deck should have been
        # used to give it the correct sign). So here we set "positive" equal
        # to the requested value. This will prevent CMOR from altering the
        # sign of the field.
        #
        # Note, this is only an issue for variables that have 'up' or 'down'
        # specified for the 'positive' parameter. For variables that don't have
        # a prescribed value of 'positive' this is not an issue.
        
        positive = d_table[cmortable]['variable_entry'][cmorvar]['positive']
        assert positive in ['up', 'down'], 'value not allowed: positive=' + positive
        d.update({'positive' : positive})
    
    # Create netcdf variable
    varid = cmor.variable(cmorvar, dv['units'], axis_ids, **d)

    # Cast data array as single precision
    ad = np.array(ad, 'float32')
    # CMOR seems to convert to single precision when it writes the netcdf file.
    # If conversion to single isn't done here then the nc metadata will say:
    #   altered by CMOR: Converted type from \'d\' to \'f\'." ;
    # Since CanESM5 is 32-bit anyways, might as well set the precision here.
    # Not sure how to set it to 64-bit (if that were ever needed).

    if write_ps:
        # Add surface pressure to output file (for model levels output)

        # zfactor_name seems to depend on what kind of time dimension is present.
        d_zfactor_time = {'time' : 'ps', 'time1' : 'ps1', 'time2' : 'ps2'}
        # 8jun.20: haven't tested ps2, just assuming it follows same rule as ps & ps1

        d = {
            'zaxis_id'          : d_cmor_axis['level'],
            'axis_ids'          : [ d_cmor_axis[dn] for dn in dims_ps ],
            #'zfactor_name'      : 'ps',
            # Using 'ps' as the name causes this CMOR error:
            #   Error: You defined variable 'ps' (table 6hrLev) with axis id 'time1' which is not
            #   part of this variable according to your table, it says: ( time latitude longitude )
            # The solution,
            #   https://github.com/PCMDI/cmip6-cmor-tables/issues/230
            # is to name it 'ps1' instead. (21 Feb 2020)
            # Note that 'ps' is the surface pressure variable listed in CMIP6_6hrLev.json.
            # 'ps1' instead is listed in CMIP6_formula_terms.json.
            #'zfactor_name'      : 'ps1',
            # Update: it seems to depend on the name of the time dimension ('time', 'time1',...)
            'zfactor_name'      : d_zfactor_time[time_ps],
            'units'             : units_ps,
        }
        
        d_cmor_zfactor['ps'] = cmor.zfactor(**d)

    cmor.write(varid, ad)


    if write_ps:
        cmor.write(d_cmor_zfactor['ps'], ps, store_with=varid)

    outfile=cmor.close(varid, file_name=True)
    cmor.close()
    
    msg('\nOutput file: ' + outfile + '\n')

    if show_elapsed_time:
        time_stop = time.time()
        t = time_stop - time_start
        msg('Time for conversion in amor.agcm_to_cmor(): ' \
            + str('%.2f' % t) + ' s (' + str('%.2g' % float(t/60.)) + ' min)')
        
    msg(' * finished agcm_to_cmor * \n')
    d_return.update({
        # return info to calling program if agcm_to_cmor() has been called via multiprocessing
        'success' : True,
        'cmor_final_filename' : cmor.get_final_filename(),
    })
    return True

def generate_surface_pressure_for_model_levs(ccc_pfx, out_suff, cmortable, source_ctabdir, surf_pres_varname='ps'):
    """
        Generate the surface pressure file necessary for conversions on AGCM model levels
    """
    #------------------------
    # get pressure level data
    #------------------------
    try:
        surf_pres_row = tu.get_vartab(source_ctabdir, cmortable)[surf_pres_varname]
    except KeyError:
        err_string =  "Trying generate intermediate surface pressure file for model level conversions "
        err_string += "in {} but {} can not be found in {}!".format(
                        cmortable, surface_pres_varname,os.path.join(source_ctabdir,cmortable))
        raise Exception(err_string)

    #---------------------------------------
    # setup for pressure level optional deck
    #---------------------------------------
    optdeck_args = {}
    optdeck_args['pfx']         = ccc_pfx
    optdeck_args['out_suff']    = out_suff
    optdeck_args['conv_realm']  = 'atmos'
    optdeck_args['optdeck']     = surf_pres_row['CCCma optional deck'].strip() 

    # input diag files
    diagfiles = surf_pres_row['CCCma diag file'].strip().lower().split() 
    optdeck_args['diagfiles'] = list(filter(None, diagfiles))

    # input ts var names
    tsvars = surf_pres_row['CCCma TS var name'].strip().split()
    tsvars = list(filter(None, tsvars))
    optdeck_args['tsvars_L'] = [ v.lower() for v in tsvars ] 

    #------------------
    # run optional deck
    #------------------
    success, outfile, logtxt = tu.run_optional_deck(**optdeck_args)

    if not success:
        err_string  = "Failed to generate the intermediate surface pressure file! "
        err_string += "Optional Deck output:\n{}".format(logtxt)
        raise Exception(err_string)
    
    # rename outfile to match that expected in agcm_to_cmor
    #   - This is likely not needed
    os.rename(outfile, "{}_{}".format(ccc_pfx,out_suff))
