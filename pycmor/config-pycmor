#!/bin/bash
# helper utility to help setup cmorization using python
#=============
# Define usage
#=============
Usage='Usage:

    config-pycmor [-c] [-h] [WORKING_DIRECTORY]
    
        Setup up directory for cmorization, bringing in the necessary files 
        from the assocaited ncconv repository.

        Positional arguments:

            WORKING_DIRECTORY : (optional) tells config-pycmor to create the conversion directory
                                            in the given WORKING_DIRECTORY. If NOT given, config-pycmor
                                            creates the converstion directory in the present 
                                            directory
        
        Flags:
           -c   : instead of linking in the variable tables and other cmor input files, copy them in instead
           -h   : display this usage message!
'

#=========
# Preamble
#=========
# Process flags
while getopts ch opt; do
    case $opt in 
        c) copy_files="yes"         ;;
        h) echo "$Usage"; exit      ;;
        ?) echo "$Usage"; exit 1    ;;
    esac
done
shift $(( OPTIND - 1 ))

# Define working directory, using the first positional argument if given
WRKDIR_NAME="netcdfconv"
if [ -z "$1" ]; then
    # no arg given, create in CWD
    WRK_DIR="$WRKDIR_NAME"
else
    if [ ! -d $1 ]; then
        mkdir -p $1
    fi
    # create in directory given at CLI
    WRK_DIR="$1/$WRKDIR_NAME"
fi

# Add a "version" number if this directory already exists
DIRVER=01
WRK_DIR0=$WRK_DIR
while [ -d ${WRK_DIR} ]; do  
    WRK_DIR=${WRK_DIR0}${DIRVER}
    DIRVER=$(echo $DIRVER | awk '{printf "%02d",$1+1}')
done

# set other useful environment variables
CWD=$(pwd)
SCRIPT_DIR=$(dirname $0)            # relative path to this script
SCRIPT_DIR=$(cd $SCRIPT_DIR && pwd) # absolute path to this script
datef=$(date +"%Y-%m-%d %T")        # date
case $(hostname) in                 # determine hall number
    hpcr3-* | *ppp3* | cs3be* ) HALLN=3 ;;
    hpcr4-* | *ppp4* | cs4be* ) HALLN=4 ;;
    hpcr5-* | *ppp5* | cs5be* ) HALLN=5 ;;
    hpcr6-* | *ppp6* | cs6be* ) HALLN=6 ;;
    * ) 
        echo "pycmor must be ran on an eccc ppp machine"
        exit 1 ;;
esac
cd $SCRIPT_DIR
NCCONV_HASH=$(git rev-parse HEAD)           # hash of ncconv
NCCONV_DIR=$(git rev-parse --show-toplevel) # absolute path of ncconv repo
cd $CWD

#=========================
# Setup Working Directory
#=========================
mkdir $WRK_DIR
cd $WRK_DIR

if [[ ! -d $NCCONV_DIR ]]; then
    echo "Failed to locate the NCCONV directory! Does $NCCONV_DIR exist?"
    exit 1
fi

# bring in pertinent files
#   - note that the callcp script requires user modification so its always copied in
cp $NCCONV_DIR/pycmor/callcp.sh . 
if [[ $copy_files == "yes" ]]; then
    cp -r $NCCONV_DIR/cmor_tools/CMIP6_CVs .
    cp -r $NCCONV_DIR/cmor_tools/cmip6-cmor-tables .
    cp $NCCONV_DIR/tables/cccma_user_input.json .
else
    ln -s $NCCONV_DIR/cmor_tools/CMIP6_CVs/ .
    ln -s $NCCONV_DIR/cmor_tools/cmip6-cmor-tables/ .
    ln -s $NCCONV_DIR/tables/cccma_user_input.json .
fi

# set version component of output file path
python $NCCONV_DIR/pycmor/set_dataset_version.py

# clone in experiment table (most up-to-date master branch) and store hash in variable
git clone git@gitlab.science.gc.ca:CanESM/cmip6_experiments.git --branch master --single-branch
EXPTAB_HASH=$(cd cmip6_experiments/ ; git rev-parse HEAD)

#===================
# Logging and Output
#===================
# Log hashes of both the current ncconv repo and any other external repos
cat > .config_pycmor.log <<EOF
config-pycmor
Date: ${datef}
-------------
ncconv            commit: $NCCONV_HASH
cmip6_experiments commit: $EXPTAB_HASH
EOF

# create script to set up env and output directions
cat > path_export.sh <<EOF
export PATH=$NCCONV_DIR/bin:$NCCONV_DIR/pycmor:\$PATH
export CMOR_WRK_DIR=$WRK_DIR
export NCCONV_DIR=$NCCONV_DIR
export HALLN=$HALLN
EOF

# print directions
echo "You should now:"
echo "  cd $WRK_DIR"
echo "  source path_export.sh"
echo "  source activate py2_cmor_v1"
echo "and then edit callcp.sh as needed and execute."
echo "For for further info, see the full documentation:"
echo "  https://goc-dx.science.gc.ca/~scrd104/documentation/pycmor/"
