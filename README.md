[![build status](https://gitlab.science.gc.ca/CanESM/ncconv/badges/pycmor/build.svg)](https://gitlab.science.gc.ca/CanESM/ncconv/commits/pycmor)

# CCCma NetCDF cmorization

## Purpose
This repository houses or points to all scripts, programs, and dependencies 
needed to convert raw model output from CanESM5 to the final, published form
including conversion to timeseries, unit conversion, and CMOR-ization.

As of December 2018, we are moving forward with a `python` implementation for
our interface with the `CMOR` software. For information on how to use/work with
this software, users are encouraged to visit the
[docs](https://goc-dx.science.gc.ca/~scrd104/documentation/pycmor/)

## Nested Repositories
This repository contains two additional nested repositories under `cmor_tools/`
    - [WCRP-CMIP's CMIP6_CVs](https://github.com/WCRP-CMIP/CMIP6_CVs), which defines
        the "Controlled Vocabulary" for CMIP6
    - [PCMDI's cmip6-cmor-tables repo](https://github.com/PCMDI/cmip6-cmor-tables), which
        uses WCRP-CMIP's repo to create functional files that are actually used
        in the data conversion

Prior to 2022-10-05, these nested repos used to be contained as sub-modules but to make
interacting with the [CanESM repository](https://gitlab.science.gc.ca/CanESM/CanESM5) easier, the
`ncconv` repo was added as a 
[git subtrees](https://medium.com/@porteneuve/mastering-git-subtrees-943d29a798ec).
Which required that these nested repos _also_ be added as subtrees. As such, these
were changed to subtrees within this repo. This allows us to easily pull in updates from
this repo into the CanESM repo.

To do this, the submodules were deleted with `git rm` and added as subtrees via:
```bash
git remote add cmip6-cmor-tables https://github.com/PCMDI/cmip6-cmor-tables.git
git fetch cmip6-cmor-tables master
git read-tree --prefix=cmor_tools/cmip6-cmor-tables -u cmip6-cmor-tables/master
git commit -m "Added cmip6-cmor-tables as a subtree"
```
and
```bash
git remote add CMIP6_CVs https://github.com/WCRP-CMIP/CMIP6_CVs.git
git fetch CMIP6_CVs master
git read-tree --prefix=cmor_tools/CMIP6_CVs -u CMIP6_CVs/master
git commit -m "Added CMIP6_CVs as a subtree"
```

### Updating the subtrees
To update subtrees we simply need to fetch from the remote
and perform a squashed subtree merge (which avoids plaguing the git history
for `ncconv` with commits from the nested repos). For example, say you want to update
the `cmip6-cmor-tables` repo - you'd do so via:
```bash
git fetch cmip6-cmor-tables BRANCH-TO-MERGE # assuming you've added the remote
git merge -s subtree --squash cmip6-cmor-tables/BRANCH-TO-MERGE
git commit
```
