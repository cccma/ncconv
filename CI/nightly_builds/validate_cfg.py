"""
    Validate configuration file. 

    This is used by validate to create plots with the nightly build data
    as part of the ci pipeline.
"""

# batch size settings
large_chunk_size = 40
small_chunk_size = 1

# batch job plot config template
template_filename = 'conf_template_verification.yaml'

# required python environment
env = 'val-env'

# data settings
run_params = {
'run'                   : 'CanESM5',
'experiment'            : 'historical',
'run_info'              : 'nightly-build',
'pycmor_version'        : 'pycmor-HEAD',
'start_date'            : '2005-01',
'end_date'              : '2005-12',
'cmip6_verification'    : True,
'direct_data_root'      : '/space/hall2/sitestore/eccc/crd/ccrn/users/scrd105/pycmor_nightly_build/CMIP6/CMIP/CCCma/CanESM5/historical',
'observations_root'     : '/space/hall2/sitestore/eccc/crd/ccrn/users/scrd104/obs4comp',
'cmip5_root'            : '/space/hall2/sitestore/eccc/crd/cetus3_data/CCCma_Output/CMIP5/output/CCCma/CanESM2/historical/',
'output_root'           : '.',
'processed_cmip5_root'  : '/space/hall2/sitestore/eccc/crd/ccrn/users/scrd104/CMIP6_PRC',
}

#list of (table, variable) pairs NOT to run CanESM2 comparisons for. 
nocomparison_list = [
('Eday',  'ta'),
('Eday',  'ua'),
('Eday',  'va'),
('Eday', 'zg'),
('Eday',  'wap'), 
('AERmonZ',  'ta'),
('AERmonZ',  'ua'),
('AERmonZ',  'va'),
('AERmonZ', 'zg'),
('AERmonZ', 'vt100'),
('EdayZ',  'ta'),
('EdayZ',  'ua'),
('EdayZ',  'va'),
('EdayZ', 'zg'),
('EdayZ', 'hus'),
('Omon', 'dissic'), 
('Omon', 'no3'), 
('Omon', 'umo'), 
('Omon', 'chl'), 
('Omon', 'detoc'), 
('Omon', 'phyc'), 
('Omon', 'pon'), 
('Omon', 'talk'), 
('Omon', 'zooc'), 
('Lmon', 'mrsos'),
]

