# Describing the NEMO grid

This document is intended to described how we define the cell bounds by four vertices

NEMO is a finite volume model that uses a C-grid stagger. Hence, one can think
of a number of different grids overlaid on each other where each point is the
center of its own rectangle. From the perspective of a thickness point (also
referred to as a tracer point, the middles of the left/right edge of this
rectangle are u-points, the top/bottom are the v-points, and the corners of the
box are vorticity/Coriolis points.

NEMO also uses a North-East indexing convention where the vertices of a cell are
referenced to the top-right (i.e. north-east)

Specific examples are given for each of the four grids where indices and numbers
use the NEMO convention and refer to the ORCA1 grid used at the CCCma. 'phi'
refers to latitude and 'lam' refers to longitude.

# T-cell vertices
The f-point of the grids are the vertices of the grid cell.

| Vertex        | Latitude          | Longitude         |
|---------------|-------------------|-------------------|
| Center        | gphit(i,j)        | glamt(i,j)        |
| Top-right     | gphif(i,j)        | glamf(i,j)        |
| Bottom-right  | gphif(i-1,j)      | glamf(i-1,j)      |
| Bottom-left   | gphif(i-1,j-1)    | glamf(i-1,j-1)    |
| Top-left      | gphif(i,j-1)      | glamf(i,j-1)      |

Example at i = 146, j = 181 (note this is 0-based indexing)
| Vertex        | Latitude          | Longitude         |
|---------------|-------------------|-------------------|
| Center        |  0.               | -106.5            |
| Top-right     | +0.166666         | -106.             |
| Bottom-right  | -0.166666         | -106.             |
| Bottom-left   | -0.166666         | -107.             |
| Top-left      | +0.166666         | -107.             |

# U-cell vertices
The v-point of the grids are the vertices of the grid cell.

| Vertex        | Latitude          | Longitude         |
|---------------|-------------------|-------------------|
| Center        | gphiu(i,j)        | glamu(i,j)        |
|               | gphiv(i,j)        | glamv(i,j)        |
|               | gphiv(i-1,j)      | glamv(i-1,j)      |
|               | gphiv(i-1,j-1)    | glamv(i-1,j-1)    |
|               | gphiv(i,j-1)      | glamv(i,j-1)      |

Example at i = 146, j = 181 (note this is 0-based indexing)
| Vertex        | Latitude          | Longitude         |
|---------------|-------------------|-------------------|
| Center        |  0.               | -106.             |
| Top-Right     | +0.166666         | -106.5            |
| Bottom-right  | -0.166666         | -106.5            |
| Bottom-left   | -0.166666         | -107.5            |
| Top-left      | +0.166666         | -107.5            |

# V-cell vertices
The U-point of the grids are the vertices of the grid cell.

| Vertex        | Latitude          | Longitude         |
|---------------|-------------------|-------------------|
| Center        | gphiv(i,j)        | glamv(i,j)        |
| Top-right     | gphiu(i,j)        | glamu(i,j)        |
| Bottom-right  | gphiu(i-1,j)      | glamu(i-1,j)      |
| Bottom-left   | gphiu(i-1,j-1)    | glamu(i-1,j-1)    |
| Top-left      | gphiu(i,j-1)      | glamu(i,j-1)      |

Example at i = 146, j = 181 (note this is 0-based indexing)
| Vertex        | Latitude          | Longitude         |
|---------------|-------------------|-------------------|
| Center        |  0.166666         | -106.5            |
| Top-right     | -0.               | -106.             |
| Bottom-right  | -0.333328         | -106.             |
| Bottom-left   | -0.333328         | -107.             |
| Top-left      | 0.                | -107.             |
