
The Conversion Process
----------------------

In order to execute a conversion, :code:`pycmor` gets information from 
a lot of different locations, and runs it all through a complex pipeline
in order to produce "CMORized" netcdf output files. As opposed to writing
a detailed description of what this pipeline is, the following flow chart
has been provided to aid users in understanding how it works.

.. figure:: imgs/pycmor_convflow.png
    :align: center
