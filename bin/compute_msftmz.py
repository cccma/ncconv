#!/usr/bin/env python
'''
Combine streatmfunctions in each basin
'''

import argparse
import subprocess
import xarray as xr
import numpy as np

invars = ['zomsfatl','zomsfipc','zomsfglo']

def get_options():
    description = ''
    parser = argparse.ArgumentParser(description=
            "Combine streamfunctions in each basin ")

    parser.add_argument('pfx', help='Filename prefix', type = str)
    for var in invars:
        parser.add_argument(var,  type = str)
    parser.add_argument('out', help='Name of the output file', type = str)

    return parser.parse_args()

if __name__ == "__main__":
    args = get_options()
    # Access all three of the heat transport components
    for var in invars:
        access_cmd = 'access {inp} {pfx}_{inp}.nc'.format(inp = var, pfx = args.pfx)
        subprocess.call(access_cmd,shell=True)
    data = xr.open_mfdataset(invars,decode_times=False).squeeze()
    # Define the dimensions of the output array
    ntime, ndepth, nlat = data[invars[0]].shape
    nbasin = 3 # 1: Atlantic, 2: Indo-Pacific 3: Global

    # Set all values to missing by default
    msftmz = np.ones((ntime,nbasin,ndepth,nlat))*1.e20
    msftmz[:,0,:,:] = data[invars[0]].values
    msftmz[:,1,:,:] = data[invars[1]].values
    msftmz[:,2,:,:] = data[invars[2]].values

    # Now make the final xarray dataset
    msftmz_var = xr.Variable(['time_counter','basin','depthw','y'],msftmz,data[invars[0]].attrs)
    # Next create an xarray dataset and then save
    coords = { coord:data[coord] for coord in data.coords if coord in ['time_counter','depthw','lat'] }
    # Rename coordinate name from lat to latitude
    coords['basin'] = np.arange(0,3) + 1
    data_vars = { }
    # Name the variable because of assumptions made across all optional decks that the first variable
    # passed in should be the output name and convert
    data_vars['zomsfatl'] = msftmz_var*(1.e6*1035)
    outdata = xr.Dataset(data_vars, coords, data.attrs)
    outdata['time_counter_bnds'] = data['time_counter_bnds']
    outdata = outdata.rename({'lat':'nav_lat'})
    outdata = outdata.isel(nav_lat=slice(0,-2))
    outdata = outdata.isel(y=slice(0,-2))
    # Mask the values which are 0. Unfortunately this is the only way to do the masking
    outdata['zomsfatl'] = outdata['zomsfatl'].where( outdata['zomsfatl'] != 0.,1.e20)
    outdata.to_netcdf('{}_{}'.format(args.pfx,args.out))
    for var in invars:
        subprocess.call('release {}'.format(var),shell=True)
