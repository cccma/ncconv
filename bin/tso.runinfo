#!/bin/bash 
#
#  Author: Yanjun Jiao
#   Usage: tso.runinfo runid [optional yearlist, ctablist, varslist in any orders]
#---------------------------------------------------------------------------------
runid=`echo $1|tr "[A-Z]" "[a-z]"`  #runid must be in lowercase
runid=${runid%/}

[[ $runid ]] || { echo " ERROR: You must specify a runid on command line"; exit; }

. $RUNPATH/ncconv/bin/tso.setup
expline=$(grep $runid $dirwrk00/cmip6_experiments/cmip6_experiment.txt|sed 's/ //g'|grep "^|$runid|")
[[ $(echo "$expline"|awk -F\| '{print NF}') == 17 ]] || { echo " ERROR: runid=$runid is not defined properly in cmip6_experiments.txt"; echo expline=$expline; exit; }

# for each following column, only the first word is valid
    mipera=CMIP6
  activity=$(echo $expline|cut -d\| -f3 |awk '{print $1}')
     modid=$(echo $expline|cut -d\| -f4 |awk '{print $1}')
     expid=$(echo $expline|cut -d\| -f5 |awk '{print $1}') 
     subid=$(echo $expline|cut -d\| -f6 |awk '{print $1}')     
     ripid=$(echo $expline|cut -d\| -f7 |awk '{print $1}')     
start_time=$(echo $expline|cut -d\| -f12|awk '{print $1}'|cut -d: -f1)
 stop_time=$(echo $expline|cut -d\| -f12|awk '{print $1}'|cut -d: -f2)
   hpcuser=$(echo $expline|cut -d\| -f13|awk '{print $1}')
   hpcmach=$(echo $expline|cut -d\| -f14|awk '{print $1}')

[[ $hpcmach == @(ppp1|hall1|ppp3|hall3) ]] && hpcmach=ppp3
[[ $hpcmach == @(ppp2|hall2|ppp4|hall4) ]] && hpcmach=ppp4

[[ $subid == none ]] || ripid=${subid}-${ripid}
dircmip6=$mipera/$activity/CCCma/$modid/$expid/$ripid
runinfo="RUNINFO: runid=$runid; mipera=$mipera; activity=$activity; modid=$modid; expid=$expid; ripid=$ripid"
char=`echo $runinfo|wc -m`

printf "%${char}s\n" |tr " " ">"
printf "%${char}s\n" "$runinfo"
printf " RUNINFO: CMIP6 Dircetory: $runid/$dircmip6 \n\n"
printf " RUNINFO: start_time=$start_time; stop_time=$stop_time; hpcuser=$hpcuser; hpcmach=$hpcmach \n"

#---------------------------------------------------
# get pycmor version and table settings
. $RUNPATH/ncconv/pycmor/pycmor.cfg

# interpret command line list to allow user to overwrite default values
yearlist=""
ctablist=""
varslist=""

if [[ $# -ge 2 ]]; then 
  shift; argslist=$@
  for args in $argslist ;  do
    if [[ $args == [0-9][0-9][0-9][0-9]*: ]]; then
      start_time=${args%:*}
    elif [[ $args == :[0-9][0-9][0-9][0-9]* ]]; then
      stop_time=${args#*:}
    elif [[ $args == [0-9][0-9][0-9][0-9]*:[0-9][0-9][0-9][0-9]* ]]; then
      start_time=${args%:*}
      stop_time=${args#*:}
    elif [[ $args == [0-9][0-9][0-9][0-9]* ]]; then
      yearlist=" ${args} $yearlist "
    elif [[ " $cmorlist " =~ " $args " ]]; then
      ctablist=" ${args} $ctablist "
    else
      # any other input is considered to be the variable name, even a typo
      varslist=" ${args} $varslist "
    fi
  done
fi
[[ ${#start_time} -eq 4 ]] && start_time=${start_time}m01
[[ ${#stop_time} -eq 4 ]] && stop_time=${stop_time}m12

#[[ " $ctablist " =~ " SImon " && " $varslist " =~ " siconc " ]] && version=v20190326

TRUE_HOST=${TRUE_HOST:=$CMCFEDEST}
[[ $TRUE_HOST == *$hpcmach ]] || { echo " ERROR: Your host machine is not hpcmach=$hpcmach"; exit; }
[[ $varslist && $(echo $ctablist|wc -w) -ne 1 ]] && { echo " ERROR: You specified a list of variables, you must specify only one CMOR table"; exit; } || :

